module uoc.entreculturas {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml.bind;
    requires java.sql;
    requires net.bytebuddy;
    requires java.persistence;
    requires com.fasterxml.classmate;

    opens uoc.entreculturas.model to java.persistance, org.hibernate.orm.core;
    opens uoc.entreculturas.controller to javafx.fxml;
    opens uoc.entreculturas to javafx.fxml, java.persistance, org.hibernate.orm.core;
    exports uoc.entreculturas.model;
    exports uoc.entreculturas.controller;
    exports uoc.entreculturas;
}