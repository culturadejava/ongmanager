package uoc.entreculturas.model;


/**
 * Creado por @autor: PabloGarcíaNúñez el 23/03/2020
 **/


import javax.persistence.*;

/**
 * Clase Administrador, hereda de Persona
 */
@Entity
@DiscriminatorValue( value = "Administrador")
public class Admin extends Persona {

   /*===================================================
                     ATRIBUTOS
    =====================================================*/

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idAdmin")
    private Long adminId ;
    @Column(name="city",nullable = false)
    private String city;

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Admin() { }

    /**
     *
     * @param nick Nick del administrador
     * @param email Dirección email del administrador
     * @param password Contraseña del administrador
     * @param city Ciudad del administrador
     */

    public Admin(String nick,String email,String password,String city){
        super(nick,email,password);
        this.city=city;
        this.adminId=adminId;
    }
}
