package uoc.entreculturas.model;

import uoc.entreculturas.model.Financiador;
import uoc.entreculturas.model.LineaAccion;
import uoc.entreculturas.model.Persona;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Entity
@DiscriminatorValue( value = "proyecto")
 /**
 * Clase Proyecto
 */
public class Proyecto {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/
    private static Long idLastProyecto=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idProyecto")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "pais")
    private String pais;

    @Column(name = "localizacion")
    private String localizacion;

    @Column(name = "acciones")
    private String acciones;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "socioLocal")
    private String socioLocal;

    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Financiador> financiadores = new ArrayList<>();

    @Transient
    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LineaAccion> listaLineas = new ArrayList<>();

    @Column(name = "fechaInicio")
    private Date fechaInicio;

    @Column(name = "fechaFin")
    private Date fechaFin;

    @Column(name = "financiacionAportada")
    private float financiacionAportada;

    @Transient
    //@OneToMany(mappedBy = "id", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Persona> listaPersonal = new ArrayList<>();

     /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Proyecto() {}

    /**
     *
     * @param codigo Código del proyecto
     * @param nombre Nombre del proyecto
     * @param pais País en donde se realiza el proyecto
     * @param localizacion Localización en la que se realiza el proyecto
     * @param acciones Acciones del proyecto
     * @param socioLocal Lista de socios locales del proyecto
     * @param financiadores Empresa o particular que financia el proyecto
     * @param listaLineas Lineas de acción del proyecto
     * @param fechaInicio Fecha de inicio del proyecto
     * @param fechaFin Deadline del proyecto
     * @param financiacionAportada Cantidad aportada a el proyecto
     * @param listaPersonal Lista de personal asociado a un proyecto
     *                      Eduardo -> En el caso práctico no aparece que debamos registrar esa información. En cualquier caso, debería estar en la clase Persona.
     */
    public Proyecto(String codigo, String nombre, String pais, String localizacion,
                    String acciones, String socioLocal, ArrayList<Financiador> financiadores, ArrayList<LineaAccion> listaLineas,
                    Date fechaInicio, Date fechaFin, float financiacionAportada, ArrayList<uoc.entreculturas.model.Persona> listaPersonal){

        this.codigo = codigo;
        this.id = idLastProyecto;
        idLastProyecto += 1;
        this.nombre = nombre;
        this.pais = pais;
        this.localizacion = localizacion;
        this.acciones = acciones;
        this.socioLocal = socioLocal;
        this.financiadores = financiadores;
        this.listaLineas = listaLineas;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.financiacionAportada = financiacionAportada;
        this.listaPersonal = listaPersonal;
    }

    /**
     *
     * @param codigo Código del proyecto
     * @param nombre Nombre del proyecto
     * @param pais País en donde se realiza el proyecto
     * @param localizacion Localización en la que se realiza el proyecto
     * @param acciones Acciones del proyecto
     * @param socioLocal Lista de socios locales del proyecto
     * @param fechaInicio Fecha de inicio del proyecto
     * @param fechaFin Deadline del proyecto
     * @param financiacionAportada Cantidad aportada a el proyecto
     */
    public Proyecto(String codigo, String nombre, String pais, String localizacion,
                    String acciones, Date fechaInicio, Date fechaFin, String socioLocal,
                    float financiacionAportada) {

        this.codigo = codigo;
        this.id = idLastProyecto;
        idLastProyecto += 1;
        this.nombre = nombre;
        this.pais = pais;
        this.localizacion = localizacion;
        this.acciones = acciones;
        this.socioLocal = socioLocal;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.financiacionAportada = financiacionAportada;
    }

    /*===================================================
                        GETTERS
    =====================================================*/
    /**
     * Método que devuelve el código del proyecto
     */
    public String getCodigo(){
        return this.codigo;
    }

    /**
     * Método que devuelve el ID del proyecto
     */
    public Long getId(){
        return this.id;
    }

    /**
     * Método que devuelve el último ID del proyecto
     */
    public static Long getIdLastProyecto(){
        return idLastProyecto;
    }

    /**
     * Método que devuelve el nombre del proyecto
     */
    public String getNombre(){
        return this.nombre;
    }

    /**
     * Método que devuelve la lista de socios Locales del proyecto
     */
    public String getSocioLocal() {
        return this.socioLocal;
    }

    /**
     * Método que devuelve el país del proyecto
     */
    public String getPais() {
        return pais;
    }

    /**
     * Método que devuelve la ubicación del proyecto
     */
    public String getLocalizacion() {
        return localizacion;
    }

    /**
     * Método que devuelve las acciones del proyecto
     */
    public String getAcciones() {
        return acciones;
    }

    /**
     * Método que devuelve la lista de financiadores del proyecto
     */
    public List<Financiador> getFinanciadores() {
        return financiadores;
    }

    /**
     * Método que devuelve la lista de lineas de acción del proyecto
     */
    public List<LineaAccion> getListaLineas() {
        return listaLineas;
    }

    /**
     * Método que devuelve la fecha de inicio del proyecto
     */
    public Date getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Método que devuelve la fecha de final del proyecto
     */
    public Date getFechaFin() {
        return fechaFin;
    }

    /**
     * Método que devuelve la financiación aportada al proyecto
     */
    public float getFinanciacionAportada() {
        return financiacionAportada;
    }

    /**
     * Método que devuelve la lista de personal asignado al proyecto
     */
    public List<uoc.entreculturas.model.Persona> getListaPersonal() {
        return listaPersonal;
    }

    @Override
    public String toString(){
        String msg = "Proyecto -> Nombre: " + this.nombre
                + "\nPaís: " + this.pais
                + "\nLocalización: " + this.localizacion
                + "\nLista de líneas:";

        for (LineaAccion linea: this.listaLineas) {
            msg = msg.concat(linea.toString());
        }
        msg = msg.concat("\nFecha inicio: " + this.fechaInicio
                + "\nFecha fin: " + this.fechaFin
                + "\nSocio local: " + this.socioLocal
                + "\nFinanciadores: ");

        for(int i = 0; i<this.financiadores.size(); i++){
            if(i>0) msg = msg.concat(" || ");
            msg = msg.concat(this.financiadores.get(i).toString());
        }

        msg = msg.concat("\nFinanciación aportada: " + this.financiacionAportada +"€"
                + "\nListaPersonal:\n");

        for (uoc.entreculturas.model.Persona persona: this.listaPersonal) {
            msg = msg.concat("\t" + persona.toString() + "\n");
        }
        msg = msg.concat("Código: " + this.codigo
                + "\nId: " + this.id +"\n");
        return msg;
    }

    /**
     *
     * @param p
     * @return
     */
    public boolean equals(Proyecto p){
        return this.codigo.equals(p.getCodigo());
    }

    /*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para asignar el código del proyecto
     * @param codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Método para asignar el id del proyecto
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    static public void setLastId(Long id) {
        idLastProyecto = id;
    }

    /**
     * Método para asignar el nombre del proyecto
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método para asignar el Pais del proyecto
     * @param pais
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Método para asignar la ubicación del proyecto
     * @param localizacion
     */
    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    /**
     * Método para asignar las acciones del proyecto
     * @param acciones
     */
    public void setAcciones(String acciones) {
        this.acciones = acciones;
    }

    /**
     * Método para asignar la fecha de inicio del proyecto
     * @param fechaInicio
     */
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * Método para asignar la fecha de final del proyecto
     * @param fechaFin
     */
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * Método para asignar la financiación asignada al proyecto
     * @param financiacionAportada
     */
    public void setFinanciacionAportada(float financiacionAportada) {
        this.financiacionAportada = financiacionAportada;
    }

    /**
     * Método para asignar el socio local del proyecto
     * @param socio
     */
    public void setSocioLocal(String socio) {
        this.socioLocal = socio;
    }

    /**
     * Método para asignar los financiadores del proyecto
     * @param financiadores ArrayList con los financiadores del proyecto
     */
    /*public void setFinanciadores(ArrayList<Financiador> financiadores) {
        this.financiadores = financiadores;
    }*/

    /*===================================================
             MÉTODOS PARA GESTIONAR FINANCIADORES
    =====================================================*/

    /**
     * Método para obtener uno de los financiadores del proyecto
     * @param id id del financiador que queremos encontrar
     */
    public Financiador getFinanciador(int id){
        Financiador financiador = null;
        for(Financiador f : this.getFinanciadores()){
            if(f.getId() == id) financiador = f;
        }
        return financiador;
    }
    /**
     * Método para añadir un financiador a la lista de financiadores del proyecto
     * @param financiador
     */
    public void addFinanciador(Financiador financiador){
        this.financiadores.add(financiador);
    }

    /**
     * Método para eliminar un financiador de la lista de financiadores del proyecto
     * @param financiador
     */
    public void eliminaFinanciador(Financiador financiador){
        this.financiadores.remove(financiador);
    }

    /*===================================================
           MÉTODOS PARA GESTIONAR LÍNEAS DE ACCIÓN
    =====================================================*/

    /**
     * Método para añadir una línea de acción a la lista de lineas de acción del proyecto
     * @param tipo
     */
    public void addLinea(String tipo){
        if(!this.esLineaDuplicada(tipo)) this.listaLineas.add(new LineaAccion(tipo));
        else System.out.println("Estás duplicando una línea de acción.");
    }

    /**
     * Método para añadir una línea de acción existente a la lista de lineas de acción del proyecto
     * @param linea
     */
    public void addLinea(LineaAccion linea){
        this.listaLineas.add(linea);
    }

    /**
     * Método para añadir una línea de acción a la lista de lineas de acción del proyecto
     * @param codigo
     */
    public LineaAccion getLinea(int codigo){
        LineaAccion linea = null;
        for(LineaAccion l : this.getListaLineas()){
            if(l.getCodigo() == codigo) linea = l;
        }
        return linea;
    }

    /**
     * Método evitar duplicidad de líneas de acción
     * @param tipo
     * @return
     */
    public boolean esLineaDuplicada(String tipo){
        Optional<LineaAccion> resultado = this.listaLineas.stream().filter(lineaAccion -> lineaAccion.getTipo().equals(tipo)).findFirst();
        return resultado.isPresent();
    }

    /**
     * Método para eliminar una línea de acción de la lista de lineas de acción del proyecto
     * @param linea
     */
    public void eliminaLinea(LineaAccion linea){
        this.listaLineas.remove(linea);
    }

    /**
     * Método para eliminar una línea de acción de la lista de lineas de acción del proyecto introduciendo su código
     * @param codigo
     */
    public void eliminaLinea(int codigo){
        this.listaLineas.removeIf(linea -> linea.getCodigo() == codigo);
    }

    /*===================================================
               MÉTODOS PARA GESTIONAR PERSONAL
    =====================================================*/

    /**
     * Método para añadir una persona a la lista de personal del proyecto
     * @param persona
     */
    public void addPersona(uoc.entreculturas.model.Persona persona){
        this.listaPersonal.add(persona);
    }

    /**
     * Método para eliminar una persona de la lista de personal del proyecto
     * @param persona
     */
    public void eliminaPersona(Persona persona){
        this.listaPersonal.remove(persona);
    }

    /*===================================================
               MÉTODO PARA AUMENTAR LAST ID
    =====================================================*/

    public static void incrementaLastId(){
        Proyecto.idLastProyecto += 1;
    }
}