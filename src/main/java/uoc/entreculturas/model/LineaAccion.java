package uoc.entreculturas.model;
import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Clase Linea de acción
 */

@Entity
@DiscriminatorValue( value = "LineaAccion")
public class LineaAccion {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/

    private static int lastCodigo = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idLíneaAcción")
    private int codigo;

    @Column(name = "nombre")
    private String tipo;

    @Transient
    //@OneToMany(mappedBy = "id", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LineaAccion> listaSublineas;

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public LineaAccion() {}

    /**
     *
     * @param tipo Tipo de línea de acción
     * @param listaSublineas Lista de sublíneas de acción
     */
    public LineaAccion(String tipo, ArrayList<LineaAccion> listaSublineas){
        this.codigo = lastCodigo;
        this.tipo = tipo;
        this.listaSublineas = listaSublineas;
        lastCodigo += 1;
    }

    /**
     *
     * @param tipo ipo de línea de acción
     */
    public LineaAccion(String tipo){
        this.codigo = lastCodigo;
        this.tipo = tipo;
        lastCodigo += 1;
        this.listaSublineas = new ArrayList<>();
    }
    /*===================================================
                        GETTERS
    =====================================================*/

    /**
     * Método que devuelve el código de la linea de acción
     * @return
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Método que devuelve el último código de línea de acción asignado
     */
    public static int getLastCodigo(){
        return lastCodigo;
    }


    /**
     * Método que devuelve el tipo de linea de acción
     * @return
     */
    public String getTipo() {
        return tipo;
    }


    /**
     * Método que devuelve la lista de sublineas de acción
     * @return
     */
    public List<LineaAccion> getListaSublineas() {
        return listaSublineas;
    }


    /**
     * Método que devuelve una sublínea en función del código dado
     * @return SublineaAcción: la sublínea de acción que queremos buscar.
     */
    public LineaAccion getSubliena(int codigo){
        LineaAccion resultado = null;
        for(LineaAccion sublinea : this.listaSublineas){
            if(sublinea.getCodigo() == codigo){
                resultado = sublinea;
            }
        }
        return resultado;
    }

    public String toString(){
        String msg = "\n\tLínea de acción, código: " + this.codigo
                + "\n\tTipo: " + this.tipo
                + "\n\tSublíneas:";
        for (LineaAccion sublinea: this.listaSublineas) {
            msg = msg.concat(sublinea.sublineaToString());
        }
        return msg;
    }

    public String sublineaToString(){
        return "\n\t\tSublínea de acción, codigo: " + this.codigo
                + "\n\t\ttipo: " + this.tipo;
    }


    /*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para actualizar el código de la linea de acción
     * @param codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Método para actualizar el lastCodigo
     * @param codigo
     */
    public static void setLastCodigo(int codigo) { LineaAccion.lastCodigo = codigo; }

    /*===================================================
               MÉTODOS PARA GESTIONAR TIPO
    =====================================================*/

    /**
     * Método para añadir un nuevo Tipo de linea de acción
     * @param tipo
     */
    public void addTipo(String tipo){
        this.tipo = this.tipo.concat(tipo);
    }

    /**
     * Método para actualizar el Tipo de linea de acción
     * @param tipo
     */
    public void actualizaTipo(String tipo){
        this.tipo = tipo;
    }

    /*===================================================
        MÉTODOS PARA GESTIONAR LAS SUBLÍNEAS DE ACCIÓN
    =====================================================*/

    /**
     * Método para añadir una sublínea de acción.
     * @param tipo texto de la sublínea.
     */
    public void addSublineaAccion(String tipo){
        if(!this.esSublineaDuplicada(tipo)) this.listaSublineas.add(new LineaAccion(tipo));
        else System.out.println("Estás duplicando una sublínea de acción.");
    }

    /**
     * Método para añadir una sublínea de acción.
     * @param linea sublínea.
     */
    public void addSublineaAccion(LineaAccion linea){
        this.listaSublineas.add(linea);
    }

    /**
     * Método para comprobar si vamos a introducir una sublínea duplicada.
     * @param tipo texto de la sublínea de acción.
     * @return true si existe, false si no.
     */
    public boolean esSublineaDuplicada(String tipo){
        Optional<LineaAccion> resultado = this.listaSublineas.stream().filter(sublineaAccion -> sublineaAccion.getTipo().equals(tipo)).findFirst();
        return resultado.isPresent();
    }

    /**
     * Método para eliminar una sublínea de acción
     * @param sublinea sublínea que queremos eliminar.
     */
    public void eliminaSublineaAccion(LineaAccion sublinea){
        this.listaSublineas.remove(sublinea);
    }

    /**
     * Método para eliminar una sublínea de acción
     * @param tipo tipo de la sublinea sublínea que queremos eliminar.
     */
    public void eliminaSublineaAccion(String tipo){
        this.listaSublineas.removeIf(sublinea -> sublinea.getTipo().equals(tipo));
    }

}
