package uoc.entreculturas.model;

/**
 * Creado por @autor: PabloGarcíaNúñez el 26/03/2020
 **/

import javax.persistence.*;

/**
 * Clase trabajador, hereda de Persona
 */
@Entity
public class Trabajador extends Persona {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/
    @Column(name="idTrabajador")
    private Long idTrabajador;
    @Column(name = "salario")
    private float salario;
    @Column(name = "telefono")
    private String telefono;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idSede")
    private Sede delegacion;



    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Trabajador(){
        super();
    }

    /**
     *
     * @param nombre Nombre del trabajador
     * @param primerapellido primer apellido del trabajador
     * @param segundoapellido primer apellido del trabajador
     * @param domicilio Dirección del trabajador
     * @param telefono Teléfono del trabajador
     * @param email Dirección email del trabajador
     * @param delegacion Delegación a la que pertenece el trabajador
     */




    public Trabajador(
            String nombre,
            String primerapellido,
            String segundoapellido,
            String domicilio,
            String telefono,
            String email,
            float salario,
            Sede delegacion)
    {
        super(nombre, primerapellido,segundoapellido, domicilio, telefono, email);
        this.salario=salario;
        this.delegacion = delegacion;
        this.idTrabajador=idTrabajador;
    }


    /*===================================================
                        GETTERS
    =====================================================*/


    /**
     * Método que devuelve el salario del trabajador
     * @return
     */
    public float getSalario(){

        return salario;
    }

    /**
     * Método que devuelve la delegación del trabajador
     * @return
     */
    public Sede getDelegacion(){

        return delegacion;
    }

    /**
     * Método que devuelve el ID del trabajador
     * @return
     */
    public Long getIdTrabajador() {
        return idTrabajador;
    }
/*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para asignar el identificador del trabajador
     * @param
     */
    public void setIdTrabajador(Long idTrabajador) {
        this.idTrabajador = idTrabajador;
    }


    /**
     * Método para asignar el salario del trabajador
     * @param salario
     */
    public void setSalario(float salario){

        this.salario=salario;
    }

    /**
     * Método para asignar la delegación del trabajador
     * @param delegacion
     */
    public void setDelegacion(Sede delegacion){

        this.delegacion=delegacion;
    }


    @Override
    public String toString() {
        return "Trabajador: \n" +
                "idTrabajador:" + idTrabajador + "\n" + super.toString()+"   "+
                "salario=" + salario + '\n';
    }
}
