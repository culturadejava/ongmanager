package uoc.entreculturas.model;

/**
 * Clase de tipo enumerado con los tipos posibles de ingreso
 */

public enum TipoIngreso {
    Cuota, Particular,Empresa,Herencia,Legado,Institución,Extraordinario, PublicoEstatal, PublicoAutonómico, PublicoEuropeo
}
