package uoc.entreculturas.model;

import javax.persistence.*;

@Entity
@DiscriminatorValue( value = "financiador")
public class Financiador {

    //@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idFinanciador")
    private int id;

    private static int lastId = 1;

    @Id
    @Column(name = "nombre")
    private String nombre;

    public Financiador(){}

    public Financiador(int id, String nom){
        this.id = id;
        this.nombre = nom;
        lastId++;
    }

    public Financiador(String nom){
        this.nombre = nom;
        this.id = lastId++;
    }

    /*===================================================
                        GETTERS
    =====================================================*/

    public int getId() {
        return this.id;
    }

    public static int getLastId() {
        return lastId;
    }

    public String getNombre() {
        return this.nombre;
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    /*===================================================
                        SETTERS
    =====================================================*/

    public void setId(int id) {
        this.id = id;
    }

    public static void setLastId(int lastId) {
        Financiador.lastId = lastId;
    }

    public static void incrementaLastId(int lastId) {
        Financiador.lastId = lastId;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
