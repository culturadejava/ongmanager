package uoc.entreculturas.model;


/**
 * Clase de tipo enumerado con las distintas periodicidades para las cuotas
 */

public enum Periodo {
    Mensual,Trimestral,Anual,Puntual
}
