package uoc.entreculturas.model;

import javax.persistence.*;

/**
 * Creado por @autor: PabloGarcíaNúñez el 26/03/2020
 **/


/**
 * Clase Colaborador, hereda de Persona
 */

@Entity
@Table(name ="Colaborador" )
public class Colaborador extends Persona {

     /*===================================================
                     ATRIBUTOS
    =====================================================*/

    @Column(name="idColaborador")
    private Long idColaborador;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idSede")
    private Sede delegacion;

     /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Colaborador(){super();}


    /**
     *
     * @param nombre Nombre del colaborador
     * @param primerApellido primer apellido del colaborador
     * @param segundoApellido primer apellido del colaborador
     * @param domicilio Dirección del colaborador
     * @param telefono Teléfono del colaborador
     * @param email Dirección email del colaborador
     * @param delegacion Delegación a la que pertenece el colaborador
     */

    public Colaborador(String nombre, String primerApellido, String segundoApellido, String domicilio, String telefono, String email,  Sede delegacion) {
        super( nombre, primerApellido, segundoApellido, domicilio, telefono, email );
        this.delegacion = delegacion;
        this.idColaborador = idColaborador;
    }



    /*===================================================
                        GETTERS
    =====================================================*/

    /**
     * Método que devuelve la delegación
     * @return
     */
    public Sede getDelegacion(){

        return delegacion;
    }

    /**
     * Método que devuelve la ID del colaborador
     * @return
     */
    public Long getIdColaborador() {
        return idColaborador;
    }

    /*===================================================
                        SETTERS
    =====================================================*/



    /**
     * Método para asignar el identificador del colaborador
     * @param idColaborador
     */

    public void setIdColaborador(Long idColaborador) {
        this.idColaborador = idColaborador;
    }

    /**
     * Método para seleccionar la delegación a la que pertenece el colaborador
     * @param delegacion
     */
    public void setDelegacion(Sede delegacion){

        this.delegacion=delegacion;
    }


    @Override
    public String toString() {
        return "Colaborador: \n" +
                "idColaborador=" + idColaborador + "\n"+
                super.toString() + "\n";
    }
}
