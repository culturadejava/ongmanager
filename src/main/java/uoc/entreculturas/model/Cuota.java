package uoc.entreculturas.model;



import javax.persistence.*;
import java.util.Date;

/**
 * Clase Cuota, hereda de Ingreso
 */

@Entity
@Table (name = "Cuota")
public class Cuota extends Ingreso {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/

    @Column(name = "idCuota")
    private Long IdCuota;

    @Column (name = "periodicidad")
    @Enumerated(value = EnumType.STRING)
    public Periodo periodicidad;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="idSocio")
    private Socio socio;



    /*===================================================
                     CONSTRUCTORES
    =====================================================*/


    /**
     *
     * @param importe Cantidad de la cuota
     * @param fecha Fecha de la cuota
     * @param tipoingreso Origen del ingreso
     */
    public Cuota(float importe, Date fecha, long idCuota, TipoIngreso tipoingreso) {
        super(importe, fecha, tipoingreso);
    }



    /**
     *  @param periodicidad Frecuencia de las aportaciones
     * @param importe Cantidad de la cuota
     * @param fecha Fecha de la cuota
     * @param tipoingreso Origen del ingreso
     * @param socio
     */
    public Cuota(Periodo periodicidad, float importe, Date fecha, long idCuota, TipoIngreso tipoingreso, Socio socio) {
        super(importe, fecha, tipoingreso);
        this.periodicidad = periodicidad;
        this.socio = socio;
    }

    public Cuota() {

    }

    /*===================================================
                        GETTERS
    =====================================================*/

    /**
     * Método que devuelve la idCuota
     * @return
     */
    public Long getIdCuota() {
        return IdCuota;
    }


    /**
     * Método que devuelve la periodicidad
     * @return
     */
    public Periodo getPeriodicidad() {
        return periodicidad;
    }

    /**
     * Método que devuelve el socio
     * @return
     */
    public Socio getSocio() {
        return socio;
    }



    /*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para seleccionar la periodicidad
     * @param periodicidad
     */
    public static void setPeriodicidad(Periodo periodicidad) {
        periodicidad = periodicidad;
    }


    /**
     * Método que asigna el Socio
     * @param socio
     */
    public void setSocio(Socio socio) {
        this.socio = socio;
    }



    /**
     * Método para mostrar los datos de cuota
     * @param
     */

    @Override
    public String toString() {
        return "Cuota{" +
                "IdCuota=" + IdCuota + super.toString() +
                ", periodicidad=" + periodicidad +
                ", socio=" + socio +
                '}';
    }



}
