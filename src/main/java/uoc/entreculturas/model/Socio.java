package uoc.entreculturas.model;


import javax.persistence.*;


/**
 * Clase socio, hereda de Persona
 */
@Entity
@Table (name = "Socio")
public class Socio extends Persona {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/

    static private Long lastSocioId = 0L;

    @Column(name = "idSocio")
    private Long socioId;

    @OneToOne (mappedBy ="socio",fetch = FetchType.LAZY)
    private Cuota cuota;

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Socio() {
        super();
        this.socioId = ++lastSocioId;
    }

    public Socio(String nombre, String primerApellido, String segundoApellido, String domicilio, String email, String telefono) {
        super(nombre, primerApellido, segundoApellido, domicilio, telefono, email);
        this.socioId = ++lastSocioId;
    }

    public Socio(String nombre, String primerApellido, String segundoApellido, String domicilio, String email, String telefono, Cuota cuota) {
        super(nombre, primerApellido, segundoApellido, domicilio, telefono, email);

        this.cuota = cuota;

        this.socioId = ++lastSocioId;
    }


    /*===================================================
                        GETTERS
    =====================================================*/

    /**
     * Método que devuelve la cuota asignada del socio
     * @return
     */
    public Cuota getCuota() {

        return this.cuota;
    }


    /**
     * Método que devuelve el Id del socio
     * @return
     */
    public Long getSocioId() {
        return this.socioId;
    }

    public static Long getLastSocioId(){
        return lastSocioId;
    }
        /*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para asignar la cuota del socio
     * @param cuota
     */


    public static void setCuota(Cuota cuota) {
    }

    /**
     * Método para asignar la id del socio
     * @param socioId
     */

    public void setSocioId(Long socioId) {
        this.socioId = socioId;
    }

    /**
     * Método para asignar el LastId del socio
     * @param id
     */

    static public void setLastId(Long id) {
        lastSocioId = id;
    }

    /**
     * Método para cancelar la cuota del socio
     */
    public void cancelCuota() {
        this.cuota = null;
    }

    @Override
    public String toString() {
        return "Socio{" +
                "idSocio= " + socioId +"  "+ super.toString()+"  "+
                ", cuota= " + cuota +
                '}';
    }
}

