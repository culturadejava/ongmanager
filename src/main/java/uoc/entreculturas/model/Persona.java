package uoc.entreculturas.model;


/**
 * Creado por @autor: PabloGarcíaNúñez el 26/03/2020
 **/

import javax.persistence.*;

/**
 * Clase Persona
 */


 @Entity
 @Inheritance(strategy = InheritanceType.JOINED)
public class Persona {

    /*===================================================
                      ATRIBUTOS
     =====================================================*/
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="idPersona")
        private Long idPersona;
        @Column(name="nombre",nullable = false)
        private String nombre;
        @Column(name="apellido1",nullable = false)
        private String primerApellido;
        @Column(name="apellido2")
        private String segundoApellido;
        @Column(name = "domicilio",nullable = false)
        private String domicilio;
        @Column(name="telefono")
        private String telefono;
        @Column(name="email")
        private String email;
        @OneToOne(mappedBy = "persona",fetch = FetchType.LAZY)
        private Usuario usuario;

    /*===================================================
                    CONSTRUCTORES
   =====================================================*/
        public Persona() { }


    public Persona(String nombre, String primerApellido, String segundoApellido) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
    }

    /**
     *
     */
    public Persona(String nombre, String primerApellido, String segundoApellido, String domicilio, String telefono, String email) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.domicilio = domicilio;
        this.telefono = telefono;
        this.email = email;
    }


     /**
      *
      */
    public Persona(String nombre, String primerApellido, String segundoApellido, String domicilio, String telefono, String email, Usuario usuario) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.domicilio = domicilio;
        this.telefono = telefono;
        this.email = email;
        this.usuario = usuario;
    }

     /*===================================================
                        GETTERS
    =====================================================*/

     /**
      * Método que devuelve el id de la persona
      * @return
      */
     public Long getIdPersona() {
         return idPersona;
     }

     /**
      * Método que devuelve el nombre de la persona
      * @return
      */
    public String getNombre(){

            return this.nombre;
    }

     /**
      * Método que devuelve el primer apellido de la persona
      * @return
      */

     public String getPrimerApellido() {
         return primerApellido;
     }


     /**
      * Método que devuelve el segundo apellido de la persona
      * @return
      */

     public String getSegundoApellido() {
         return segundoApellido;
     }


     /**
      * Método que devuelve la dirección de la persona
      * @return
      */
    public String getDomicilio(){

        return this.domicilio;
    }

     /**
      * Método que devuelve el teléfono de la persona
      * @return
      */
    public String getTelefono(){

        return this.telefono;
    }

     /**
      * Método que devuelve el email de la persona
      * @return
      */
    public String getEmail(){

        return this.email;
    }

     /**
      * Método que devuelve el usuario de la persona
      * @return
      */
    public Usuario getUsuario(){

            return usuario;
    }




     /*===================================================
                        SETTERS
    =====================================================*/


     /**
      * Método para asignar el nombre de la persona
      * @param nombre
      */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

     /**
      * Método para asignar el primer apellido de la persona
      * @param primerApellido
      */
     public void setPrimerApellido(String primerApellido) {
         this.primerApellido = primerApellido;
     }

     /**
      * Método para asignar el segundo apellido de la persona
      * @param segundoApellido
      */
     public void setSegundoApellido(String segundoApellido) {
         this.segundoApellido = segundoApellido;
     }

     /**
      * Método para asignar la dirección de la persona
      * @param domicilio
      */
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

     /**
      * Método para asignar el teléfono de la persona
      * @param telefono
      */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

     /**
      * Método para asignar el email de la persona
      * @param email
      */
    public void setEmail(String email) {
        this.email = email;
    }

     /**
      * Método para asignar el usuario de la persona
      * @param usuario
      */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }



    @Override
    public String toString() {
        return "nombre='" + nombre + "   " +
                "apellido1='" + primerApellido + "   " +
                "apellido2='" + segundoApellido + "   " +
                "domicilio='" + domicilio + "   " +
                "telefono='" + telefono + "   " +
                "email='" + email;
    }

}
