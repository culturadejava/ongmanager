package uoc.entreculturas.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Sede {

    /*===================================================
                ATRIBUTOS
    =====================================================*/
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSede")
    private Long id;

    @Column(name = "location")
    private String location;

    @Column(name = "isCentral")
    private Boolean isCentral;

    @OneToMany()
    @JoinColumn(name = "idSede")
    private List<Trabajador> trabajadores = new ArrayList<>();

    @OneToMany()
    @JoinColumn(name = "idSede")
    private List<Colaborador> colaboradores = new ArrayList<>();

    @OneToMany()
    @JoinColumn(name = "idSede")
    private List<Voluntario> voluntarios = new ArrayList<>();

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Sede() {
    }

    /**
     *
     * @param id Id de la sede
     * @param isCentral Si la sede es central o no
     * @param location Dirección de la sede
     */
    public Sede(Long id, Boolean isCentral, String location) {
        this.setId(id);
        this.setIsCentral(isCentral);
        this.setLocation(location);
    }

    /**
     *
     * @param isCentral Si la sede es central o no
     * @param location Dirección de la sede
     */
    public Sede(Boolean isCentral, String location) {
        this.setIsCentral(isCentral);
        this.setLocation(location);
    }

    /**
     *
     * @param localizacion Dirección de la sede
     */
    public Sede(String localizacion) {
        this.setLocation(localizacion);
    }


     /*===================================================
                        GETTERS
    =====================================================*/


    /**
     * Método que devuelve la ID de la sede
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Método que devuelve la ubicación de la sede
     * @return
     */
    public String getLocation() {
        return location;
    }


    /*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para asignar la ID a la sede
     * @return
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     /**
     * Método para detectar si la central es sede o no
     * @return
     */
    public Boolean getIsCentral() {
        return this.isCentral;
    }
    /**
     * Método para asignar si la sede es Central o no
     * @param isCentral
     */
    public void setIsCentral(Boolean isCentral) {
        this.isCentral = isCentral;
    }

    /**
     * Método para asignar la ubicación de la sede
     * @param localizacion
     */
    public void setLocation(String localizacion) {
        this.location = localizacion;
    }


    public List<Trabajador> getTrabajadores() {
        return this.trabajadores;
    }

    public List<Colaborador> getColaboradores() {
        return this.colaboradores;
    }

    public List<Voluntario> getVoluntarios() {
        return this.voluntarios;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.getLocation();
    }
}
