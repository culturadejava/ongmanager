package uoc.entreculturas.model;


import javax.persistence.*;
import java.util.Date;

/**
 * Clase Ingreso
 */
@Entity
@Table (name = "Ingreso")
@Inheritance(strategy = InheritanceType.JOINED)
public class Ingreso {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/

    static Integer idLastIngreso = 0;

    @Column (name = "importe")
    private Float importe;

    @Column (name = "fecha")
    private Date fecha;

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "idIngreso")
    private Integer idIngreso;

    @Column (name ="tipoIngreso")
    @Enumerated(value = EnumType.STRING)
    private TipoIngreso tipoIngreso;

    public Ingreso(float importe, Date fecha, uoc.entreculturas.model.TipoIngreso tipoingreso) {}

     /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    /**
     *
     * @param importe Importe del ingreso
     * @param fecha Fecha del ingreso
     * @param tipoIngreso Tipo de ingreso
     */
    public Ingreso(Float importe, Date fecha, TipoIngreso tipoIngreso) {
        this.importe = importe;
        this.fecha = fecha;
        this.tipoIngreso = tipoIngreso;
        this.idIngreso = idLastIngreso++;
    }

    public Ingreso() {

    }

    /*===================================================
                        GETTERS
    =====================================================*/

    /**
     * Método que devuelve el importe del ingreso
     * @return
     */
    public float getImporte() {
        return importe;
    }


    /**
     * Método que devuelve la ID del último ingreso
     * @return
     */
    public int getIdIngreso() {
        return idIngreso;
    }

    /**
     * Método que devuelve la fecha del ingreso
     * @return
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * Método que devuelve el tipo de ingreso
     * @return
     */
    public TipoIngreso getTipoIngreso() {
        return tipoIngreso;
    }

     /*===================================================
                        SETTERS
    =====================================================*/

    /**
     * Método para seleccionar el importe del ingreso
     * @param importe
     */
    public static void setImporte(float importe) {
    }


    /**
     * Método para seleccionar ID del último ingreso
     * @param idLastIngreso
     */
    public void setIdIngreso(int idLastIngreso) {
        this.idIngreso = idIngreso;
    }

    /**
     * Método para seleccionar la fecha del ingreso
     * @param fecha
     */
    public static void setFecha(Date fecha) {
    }

    /**
     * Método para seleccionar el tipo de ingreso
     * @param tipoIngreso
     */
    public static void setTipoIngreso(TipoIngreso tipoIngreso) {
        tipoIngreso = tipoIngreso;
    }

    /**
     * Método para presentar los datos de ingreso
     * @param
     */

    @Override
    public String toString() {
        return "Ingreso{" +
                "importe=" + importe +
                ", fecha=" + fecha +
                ", idIngreso=" + idIngreso +
                ", tipoIngreso=" + tipoIngreso +
                '}';
    }
}
