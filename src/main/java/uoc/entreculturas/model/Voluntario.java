package uoc.entreculturas.model;

import javax.persistence.*;

/**
 * Creado por @autor: PabloGarcíaNúñez el 26/03/2020
 **/


/**
 * Clase voluntario, hereda de persona
 */



@Entity
@Table(name="Voluntario")
public class Voluntario extends Persona {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/
    @Column(name = "idVoluntario")
    public Long idVoluntario;
    @Column(name="nacionalidad",nullable = false)
    String nacionalidad;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idSede")
    Sede delegacion;

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Voluntario(){super();}

    /**
     *
     * @param nombre Nombre del trabajador
     * @param primerApellido primer apellido del trabajador
     * @param segundoApellido primer apellido del trabajador
     * @param domicilio Dirección del trabajador
     * @param telefono Teléfono del trabajador
     * @param email Dirección email del trabajador
     * @param delegacion Delegación a la que pertenece el trabajador
     */


    public Voluntario(String nombre, String primerApellido, String segundoApellido, String domicilio, String telefono, String email, String nacionalidad, Sede delegacion) {
        super( nombre, primerApellido, segundoApellido, domicilio, telefono, email );
        this.nacionalidad = nacionalidad;
        this.delegacion = delegacion;
        this.idVoluntario=idVoluntario;
    }



    /*===================================================
                        GETTERS
    =====================================================*/

    /**
     * Método que devuelve la nacionalidad del voluntario
     * @return
     */
    public String getNacionalidad(){

        return nacionalidad;
    }

    /**
     * Método que devuelve la delegación del voluntario
     * @return
     */
    public Sede getDelegacion(){

        return delegacion;
    }

    /**
     * Método que devuelve la ID del voluntario
     * @return
     */
    public Long getIdVoluntario() {
        return idVoluntario;
    }
/*===================================================
                        SETTERS
    =====================================================*/


    /**
     * Método para asignar el identificador del trabajador
     * @param idVoluntario
     */

    public void setIdVoluntario(Long idVoluntario) {
        this.idVoluntario = idVoluntario;
    }

    /**
     * Método para asignar la nacionalidad del voluntario
     * @param nacionalidad
     */
    public void setNacionalidad(String nacionalidad){

        this.nacionalidad=nacionalidad;
    }

    /**
     * Método para asignar la delegación del voluntario
     * @param delegacion
     */
    public void setDelegacion(Sede delegacion){
        this.delegacion=delegacion;

    }


    @Override
    public String toString() {
        return "Voluntario:\n" +
                "idVoluntario=" + idVoluntario + "\n" +
                super.toString() + "   "+
                "nacionalidad='" + nacionalidad + '\n';
    }
}
