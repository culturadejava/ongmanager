package uoc.entreculturas.model;


/**
 * Creado por @autor: PabloGarcíaNúñez el 23/03/2020
 **/


import javax.persistence.*;
import java.util.Objects;

/**
 * Clase usuario
 */

@Entity
public class Usuario {

    /*===================================================
                     ATRIBUTOS
    =====================================================*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idUsuario")
    private Long idUsuario;
    @Column(name = "nick",nullable = false)
    private String nick;
    @Column(name = "emailONG")
    private String email;
    @Column(name = "password",nullable = false)
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="idPersona")
    private Persona persona;


    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public Usuario(){}

    public Usuario(String nick, String email, String password) {
        this.nick = nick;
        this.email = email;
        this.password = password;
    }



    public Usuario(String nick, String email, String password, Persona persona) {
        this.nick = nick;
        this.email = email;
        this.password = password;
        this.persona = persona;
    }


   /*===================================================
                        GETTERS
    =====================================================*/


    /**
     * Método que devuelve el nick del usuario
     * @return
     */
    public String getNick(){
        return this.nick;
        }

    /**
     * Método que devuelve el email del usuario
     * @return
     */
    public String getEmail(){

        return this.email;
    }

    /**
     * Método que devuelve la contraseña del usuario
     * @return
     */
    public String getPassword(){

        return this.password;
    }

    /**
     * Método que devuelve el ID del usuario
     * @return
     */
    public Long getIdUsuario(){

        return this.idUsuario;
    }


    /**
     * Método que devuelve el objeto persona asociado al usuario
     * @return
     */

    public Persona getPersona(){
        return this.persona;
    }

    /*===================================================
                        SETTERS
    =====================================================*/


    /**
     * Método para asignar la contraseña del usuario
     * @param mipassword
     */
    public void setPassword(String mipassword){

        password=mipassword;
    }

    /**
     * Método para asignar el nick del usuario
     * @param nick
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * Método para asignar la email del usuario
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Método para asignar el tipo de persona al usuario
     * @param persona
     */

    public void setPersona(Persona persona) {
        this.persona = persona;
        persona.setUsuario(this);
    }



    /*===================================================
                        MÉTODOS
    =====================================================*/


    @Override
    public String toString() {
        return "Usuario{" +
                "nick='" + nick + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", persona=" + persona +
                ", idUsuario=" + idUsuario +
                '}';
    }


    /**
     * Metodo que compara si un objeto usuario es igual a otro en función de su nick y password
     */
    @Override
    public boolean equals(Object o) {
        boolean res;
        if((o!=null)&&(o instanceof Usuario)){
            res=((this.nick==((Usuario) o).getNick())&&(this.password==((Usuario) o).getPassword()));
        }else{
            return false;
        }
        return res;
    }


}


