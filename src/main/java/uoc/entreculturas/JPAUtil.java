package uoc.entreculturas;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Creado por @autor: PabloGarcíaNúñez el 15/05/2020
 **/

public class JPAUtil {

    private static final String PERSISTENCE_UNIT_NAME="entreculturasUnit";
    private static EntityManagerFactory emf;

    public static EntityManagerFactory getEmf(){
        if (emf==null){
            emf= Persistence.createEntityManagerFactory( (PERSISTENCE_UNIT_NAME) );
        }
        return emf;
    } //end getEmf()

    public static void shutdown()
    {
        if (emf!=null){

            emf.close();
        }
    } //end shutdown()
}
