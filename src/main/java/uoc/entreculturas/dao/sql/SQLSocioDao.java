package uoc.entreculturas.dao.sql;

import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.dao.Dao;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.model.Socio;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;
import java.util.List;

public class SQLSocioDao implements Dao<Socio> {

    EntityManagerFactory factory = JPAUtil.getEmf();

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public SQLSocioDao(){

    }

    @Override
    public Socio get(Long socioId) throws JAXBException, SQLException, DaoException {
        Socio socio=null;
        try {
            System.out.println("Creando el entity manager");
            EntityManager em = factory.createEntityManager();
            System.out.println("Recogiendo el socio");
            TypedQuery<Socio> misocio=em.createQuery("select s from Socio s where s.socioId=:idsocio",Socio.class);
            misocio.setParameter("idsocio",socioId);
            socio=(Socio)misocio.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
        }

        return socio;
    }

    @Override
    public List<Socio> getAll() throws JAXBException, SQLException, DaoException {
        try {
            EntityManager em = factory.createEntityManager();
            TypedQuery<Socio> query = em.createQuery("SELECT socio FROM Socio socio", Socio.class);
            List<Socio> results = query.getResultList();
            return results;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    @Override
    public Socio save(Socio socio) throws JAXBException, SQLException, DaoException {
        return null;
    }

    @Override
    public Socio update(Socio socio) throws JAXBException, SQLException, DaoException {

        try {
            EntityManager em = factory.createEntityManager();
            Socio socioSQL = em.find(Socio.class, socio.getSocioId());
            em.getTransaction().begin();

            socioSQL.setSocioId(socio.getSocioId());
            socioSQL.setNombre(socio.getNombre());
            socioSQL.setPrimerApellido(socio.getPrimerApellido());
            socioSQL.setSegundoApellido(socio.getSegundoApellido());
            socioSQL.setDomicilio(socio.getDomicilio());
            socioSQL.setTelefono(socio.getTelefono());
            socioSQL.setEmail(socio.getEmail());


            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        }
        return socio;
    }

    @Override
    public Socio delete(Socio socio) throws JAXBException, SQLException, DaoException, JPAException {

        EntityManager em = factory.createEntityManager();
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }

            em.createQuery("delete from Socio where socioId = :idSocio").setParameter("idSocio",socio.getSocioId()).executeUpdate();

            em.getTransaction().commit();
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }

        return socio;
    }
}
