package uoc.entreculturas.dao.sql;


import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.dao.*;
import uoc.entreculturas.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.xml.bind.JAXBException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLSedeDAO implements Dao<Sede> {
    EntityManagerFactory factory = JPAUtil.getEmf();

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public SQLSedeDAO() {

    }

    @Override
    public Sede get(Long id) throws JAXBException, SQLException, DaoException {
        Sede sede = null;

        try {
            System.out.println("Creando el entitiy manager");
            EntityManager em = factory.createEntityManager();
            System.out.println("Recogiendo la sede");
            sede = (Sede) em.find(Sede.class, id);
        } catch (Exception e) {
            System.out.println(e);
        }

        return sede;
    }

    @Override
    public List<Sede> getAll() throws JAXBException, SQLException, DaoException {
        try {
            EntityManager em = factory.createEntityManager();
            TypedQuery<Sede> query = em.createQuery("SELECT sede FROM Sede sede", Sede.class);
            List<Sede> results = query.getResultList();
            return results;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    public Sede setCentral(Sede sede) throws JAXBException, SQLException, DaoException {
        if (sede.getIsCentral()) {
            try {
                EntityManager em = factory.createEntityManager();
                TypedQuery<Sede> query = em.createQuery("SELECT sede FROM Sede sede WHERE isCentral = true", Sede.class);
                List<Sede> results = query.getResultList();

                results.forEach(result -> {
                    result.setIsCentral(false);
                    try {
                        this.update(result);
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } catch (DaoException e) {
                        e.printStackTrace();
                    }
                });
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        return sede;
    }

    @Override
    public Sede save(Sede sede) throws JAXBException, SQLException, DaoException {
        this.setCentral(sede);

        try {
            EntityManager em = factory.createEntityManager();
            em.getTransaction().begin();
            em.persist(sede);
            em.getTransaction().commit();
            return sede;
        } catch (Exception e) {
            System.out.println(e);
        }

        return sede;
    }

    @Override
    public Sede update(Sede sede) throws JAXBException, SQLException, DaoException {
        this.setCentral(sede);

        try {
            EntityManager em = factory.createEntityManager();
            Sede sedeSQL = em.find(Sede.class, sede.getId());
            em.getTransaction().begin();
            sedeSQL.setLocation(sede.getLocation());
            sedeSQL.setIsCentral(sede.getIsCentral());
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        }

        return sede;
    }

    @Override
    public Sede delete(Sede sede) throws JAXBException, SQLException, DaoException, JPAException {
        EntityManager em = factory.createEntityManager();
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }

            em.createQuery("delete from Sede where idSede= :id").setParameter("id",sede.getId()).executeUpdate();

            em.getTransaction().commit();
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }

        return sede;
    }
}
