package uoc.entreculturas.dao.sql;

import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.dao.Dao;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.model.Proyecto;
import uoc.entreculturas.model.Sede;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.xml.bind.JAXBException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class SQLProyectoDao  implements Dao<Proyecto> {
    EntityManagerFactory factory = JPAUtil.getEmf();

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public SQLProyectoDao(){

    }

    @Override
    public Proyecto get(Long id) throws JAXBException, SQLException, DaoException {
        Proyecto proyecto = null;

        try {
            System.out.println("Creando el entitiy manager");
            EntityManager em = factory.createEntityManager();
            System.out.println("Recogiendo el proyecto");
            proyecto = (Proyecto) em.find(Proyecto.class, id);
        } catch (Exception e) {
            System.out.println(e);
        }

        return proyecto;
    }

    @Override
    public List<Proyecto> getAll() throws JAXBException, SQLException, DaoException {
        try {
            EntityManager em = factory.createEntityManager();
            TypedQuery<Proyecto> query = em.createQuery("SELECT proyecto FROM Proyecto proyecto", Proyecto.class);
            List<Proyecto> results = query.getResultList();
            return results;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    @Override
    public Proyecto save(Proyecto proyecto) throws JAXBException, SQLException, DaoException {
        return null;
    }

    @Override
    public Proyecto update(Proyecto proyecto) throws JAXBException, SQLException, DaoException {

        try {
            EntityManager em = factory.createEntityManager();
            Proyecto proyectoSQL = em.find(Proyecto.class, proyecto.getId());
            em.getTransaction().begin();

            proyectoSQL.setNombre(proyecto.getNombre());
            proyectoSQL.setCodigo(proyecto.getCodigo());
            proyectoSQL.setId(proyecto.getId());
            proyectoSQL.setAcciones(proyecto.getAcciones());
            proyectoSQL.setFechaFin(proyecto.getFechaFin());
            proyectoSQL.setFechaInicio(proyecto.getFechaInicio());
            proyectoSQL.setFinanciacionAportada(proyecto.getFinanciacionAportada());
            proyectoSQL.setLocalizacion(proyecto.getLocalizacion());
            proyectoSQL.setPais(proyecto.getPais());
            proyectoSQL.setSocioLocal(proyecto.getSocioLocal());

            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
        }

        return proyecto;
    }

    @Override
    public Proyecto delete(Proyecto proyecto) throws JAXBException, SQLException, DaoException, JPAException {
        EntityManager em = factory.createEntityManager();
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }

            em.createQuery("delete from Proyecto where idProyecto= :id").setParameter("id",proyecto.getId()).executeUpdate();

            em.getTransaction().commit();
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }

        return proyecto;
    }
}
