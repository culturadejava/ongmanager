package uoc.entreculturas.dao;

import uoc.entreculturas.JPAException;

import javax.xml.bind.JAXBException;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface con acciones del patrón DAO
 * @param <T>
 */
public interface Dao<T> {

    T get(Long id) throws JAXBException, SQLException, DaoException;

    List<T> getAll() throws JAXBException, SQLException, DaoException;

    T save(T t) throws JAXBException,SQLException, DaoException;

    T update(T t) throws JAXBException,SQLException, DaoException;

    T delete(T t) throws JAXBException,SQLException, DaoException, JPAException;
}
