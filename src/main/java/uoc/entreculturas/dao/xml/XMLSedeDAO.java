package uoc.entreculturas.dao.xml;

import uoc.entreculturas.dao.Dao;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.model.Sede;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Clase DAO de Sede
 */
public class XMLSedeDAO implements Dao<Sede> {

    /*===================================================
                    ATRIBUTOS
   =====================================================*/

    /*===================================================
                     CONSTRUCTORES
    =====================================================*/

    public XMLSedeDAO() {
        super();
    }

    @Override
    public Sede get(Long id) throws JAXBException, SQLException, DaoException {
        return null;
    }

    @Override
    public List<Sede> getAll() throws JAXBException, SQLException, DaoException {
        return null;
    }

    @Override
    public Sede save(Sede sede) throws JAXBException, SQLException, DaoException {
        return null;
    }

    @Override
    public Sede update(Sede sede) throws JAXBException, SQLException, DaoException {
        return null;
    }

    @Override
    public Sede delete(Sede sede) throws JAXBException, SQLException, DaoException {
        return null;
    }
}
