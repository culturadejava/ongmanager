package uoc.entreculturas.dao;

import uoc.entreculturas.dao.sql.*;

import java.sql.SQLException;

public class SQLDaoFactory extends DaoFactory {

    @Override
    public Dao getDao(String entity) throws SQLException {
        switch (entity) {
            case "Sede":
                return new SQLSedeDAO();
            case "Proyecto":
                return new SQLProyectoDao();
            case "Socio":
                return new SQLSocioDao();
            default:
                return null;
        }
    }

}
