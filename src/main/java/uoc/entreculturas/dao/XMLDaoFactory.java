package uoc.entreculturas.dao;

import uoc.entreculturas.dao.xml.*;

public class XMLDaoFactory extends DaoFactory{
     public  Dao getDao(String entity) {
        switch (entity) {
            case "Sede":
                return new XMLSedeDAO();
            default:
                    return null;
                    }
              }
}



