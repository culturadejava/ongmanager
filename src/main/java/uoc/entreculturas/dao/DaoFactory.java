package uoc.entreculturas.dao;

import java.sql.SQLException;

public abstract class DaoFactory {

    //Debe haber un método abstracto por cada DAO que se pueda crear

    public abstract Dao getDao(String entity) throws SQLException;


    public static DaoFactory getDaoFactory(String DaoType) {
        switch(DaoType) {
            case "XML":
                return new XMLDaoFactory();
            case "SQL":
                return new SQLDaoFactory();
            default:
                return null;
        }
    }
}//fin clase DaoFactory




