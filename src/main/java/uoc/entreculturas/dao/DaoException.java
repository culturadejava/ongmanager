package uoc.entreculturas.dao;

/**
 * Creado por @autor: PabloGarcíaNúñez el 24/04/2020
 **/
public class DaoException extends Exception{

    /*===================================================
                    CONSTRUCTORES
   =====================================================*/

    public DaoException(String message){

        super(message);
    }


    public DaoException(String message, Throwable cause){
        super(message,cause);

    }

    public DaoException (Throwable cause){

        super(cause);
    }



}
