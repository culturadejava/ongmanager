package uoc.entreculturas.controller;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.dao.DaoFactory;
import uoc.entreculturas.dao.sql.SQLProyectoDao;
import uoc.entreculturas.model.Proyecto;
import uoc.entreculturas.model.Sede;
import uoc.entreculturas.service.NavigationService;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

public class ProyectoTableController implements Initializable {
    private SQLProyectoDao proyectosDao = (SQLProyectoDao) DaoFactory.getDaoFactory("SQL").getDao("Proyecto");

    public ProyectoTableController() throws SQLException {
    }



    public class Item {

        public SimpleLongProperty id = new SimpleLongProperty();
        public SimpleStringProperty nombre = new SimpleStringProperty();
        public SimpleStringProperty codigo = new SimpleStringProperty();

        public Long getId() {
            return id.get();
        }

        public String getNombre() {
            return nombre.get();
        }

        public String getCodigo() {
            return codigo.get();
        }
    }

    @FXML
    TableView<ProyectoTableController.Item> itemTbl;

    @FXML
    TableColumn itemId;
    @FXML
    TableColumn itemName;
    @FXML
    TableColumn itemCodigo;
    @FXML
    TableColumn itemEdit;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        // Set up the table data
        itemId.setCellValueFactory(
                new PropertyValueFactory<ProyectoTableController.Item,Long>("id")
        );
        itemName.setCellValueFactory(
                new PropertyValueFactory<ProyectoTableController.Item,String>("nombre")
        );
        itemCodigo.setCellValueFactory(
                new PropertyValueFactory<ProyectoTableController.Item,String>("codigo")
        );

        addButtonToTable();

        ObservableList<Item> data = FXCollections.observableArrayList();

        DateFormat formatter=new SimpleDateFormat("dd/MM/yyyy");
        try {
            for (Proyecto proyecto : this.proyectosDao.getAll()) {
                Item item = new Item();
                item.id.setValue(proyecto.getId());
                item.nombre.setValue(proyecto.getNombre());
                item.codigo.setValue(proyecto.getCodigo());
                data.add(item);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        itemTbl.setItems(data);
    }

    public void addButtonToTable() {
        Callback<TableColumn<ProyectoTableController.Item, Void>, TableCell<ProyectoTableController.Item, Void>> cellFactory =
                new Callback<TableColumn<ProyectoTableController.Item, Void>, TableCell<ProyectoTableController.Item, Void>>() {
                    @Override
                    public TableCell<ProyectoTableController.Item, Void> call(final TableColumn<ProyectoTableController.Item, Void> param) {
                        final TableCell<ProyectoTableController.Item, Void> cell = new TableCell<ProyectoTableController.Item, Void>() {

                            private final Button btn = new Button("Action");

                            {
                                btn.setOnAction((ActionEvent event) -> {
                                    ProyectoTableController.Item data = getTableView().getItems().get(getIndex());
                                    try {
                                        changeScreenButtonPushed(event, data.getId());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (DaoException e) {
                                        e.printStackTrace();
                                    } catch (JAXBException e) {
                                        e.printStackTrace();
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                });
                            }

                            @Override
                            public void updateItem(Void item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                } else {
                                    setGraphic(btn);
                                }
                            }
                        };
                        return cell;
                    }
                };

        itemEdit.setCellFactory(cellFactory);
    }

    /**
     * When this method is called, it will change the Scene to
     * a TableView example
     */
    public void changeScreenButtonPushed(ActionEvent event, Long id) throws IOException, DaoException, JAXBException, SQLException {
        URL url = new File("src/main/java/uoc/entreculturas/view/ProyectoView.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent proyectoView = loader.load();

        ProyectoController controller = loader.getController();
        controller.setProyecto(id);

        Scene tableViewScene = new Scene(proyectoView);

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void goHome(ActionEvent event) throws IOException {
        NavigationService.goHome(event);
    }
}
