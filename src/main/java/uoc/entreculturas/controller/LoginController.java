package uoc.entreculturas.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.model.*;
import uoc.entreculturas.service.NavigationService;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML private TextField txtUsername;
    @FXML private TextField txtPassword;
    @FXML private TextArea resultArea;

    @FXML private Button loginButton;
    @FXML private Button ExitButton;




    @Override
    public void initialize(URL location, ResourceBundle resources) {

        resultArea.setText("Introduce tu nombre de Usuario y Password para registrarte en el sistema."+
                "\nClica en el boton de salir si no quieres continuar."+
                "\nPuedes probar a hacer el login con:\nroot:root"+
                "\n"+
                "\nO como trabajador con:"+
                "\nsaravar:sarvar");
        loginButton.setDisable(true);

    }

    @FXML
    public void enableLoginButtom(){
        String username=txtUsername.getText();
        String password=txtPassword.getText();
        boolean isDisable= (username.isEmpty() || username.trim().isEmpty()) || (password.isEmpty() || password.trim().isEmpty());
        loginButton.setDisable(isDisable);
    }

    private Boolean checkPassword(Usuario usuario) {
        return usuario.getPassword().equals(txtPassword.getText());
    }

    @FXML
    public void loginUsuario(ActionEvent event) throws JPAException {

        final String USERCHECK="select u from Usuario u WHERE u.nick=:username";
        final String GETTRABAJADOR="select t from Trabajador t WHERE t.idPersona=:idpersona";

        EntityManager em= JPAUtil.getEmf().createEntityManager();
        String username=txtUsername.getText();
        try{
            if(!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            TypedQuery<Usuario> misuario= em.createQuery(USERCHECK,Usuario.class);
            misuario.setParameter("username",username);
            Usuario usuario=(Usuario)misuario.getSingleResult();
            em.getTransaction().commit();

            em.getTransaction().begin();
            Persona persona = usuario.getPersona();

            if (this.checkPassword(usuario)) {
                if (persona instanceof Admin) {

                    OpenAdminGUI(event);
                }
                if (persona instanceof Trabajador) {
                    Long idpersona = persona.getIdPersona();
                    TypedQuery<Trabajador> mitrabajador = em.createQuery(GETTRABAJADOR, Trabajador.class);
                    mitrabajador.setParameter("idpersona", idpersona);
                    Trabajador trabajador = mitrabajador.getSingleResult();
                    em.getTransaction().commit();
                    OpenTrabajadorGUI(trabajador, event);
                }
                if ((persona instanceof Socio)){
                    resultArea.setText("Pendiente implementación lanzamiento menu Socio");

                }
            } else {
                resultArea.setText("La contraseña no es correcta");
            }

        } catch (NoResultException exception){

            resultArea.setText("Este usuario no está registrado, modifica los valores."+
                    "\nClica en el boton de salir si no quieres continuar");
            txtUsername.setText(null);
            txtPassword.setText(null);

        } catch (Exception e) {
            throw new JPAException(e.getMessage(),e);
        } finally {
            em.close();
        }
    }


    @FXML
    public void WindowClose(ActionEvent event)  {
        JPAUtil.shutdown();
        Stage stage=(Stage) ExitButton.getScene().getWindow();
        stage.close();
    }


    public Stage OpenTrabajadorGUI(Trabajador trabajador, ActionEvent event) throws IOException {

        URL url = new File("src/main/java/uoc/entreculturas/view/TrabajadorMenuView.fxml").toURI().toURL();
        FXMLLoader fxmlLoader=new FXMLLoader(url);
        Parent sedeView = fxmlLoader.load();

        TrabajadorMenuController controller = fxmlLoader.getController();
        controller.setTrabajadorDataToMenu(trabajador);

        Scene tableViewScene = new Scene(sedeView);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

        return window;
    }


    public void OpenAdminGUI(ActionEvent event) throws IOException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/AdminMenuView.fxml", event);
    }


}
