package uoc.entreculturas.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.model.Colaborador;
import uoc.entreculturas.model.Sede;
import uoc.entreculturas.model.Voluntario;
import uoc.entreculturas.service.NavigationService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Creado por @autor: PabloGarcíaNúñez el 15/05/2020
 **/
public class VoluntarioController implements Initializable {

    @FXML
    private TextField txtNombre;
    @FXML private TextField txtApellido1;
    @FXML private TextField txtApellido2;
    @FXML private TextField txtDomicilio;
    @FXML private TextField txtTelefono;
    @FXML private TextField txtEmail;
    @FXML private TextField txtNacionalidad;
    @FXML private ComboBox< Sede > sedeComboBox;
    @FXML private TextField txtIdVoluntario;
    @FXML private TextArea resultConsole;
    @FXML private TableView< Voluntario > VoluntarioTableView;
    @FXML private TableColumn<Voluntario,Long> clmnId_Voluntario;
    @FXML private TableColumn<Voluntario,String> clmnNombre;
    @FXML private TableColumn<Voluntario,String> clmnApellido1;
    @FXML private TableColumn<Voluntario,String> clmnApellido2;
    @FXML private TableColumn<Voluntario,String> clmnDomicilio;
    @FXML private TableColumn<Voluntario,String> clmnTelefono;
    @FXML private TableColumn<Voluntario,String> clmnEmail;
    @FXML private TableColumn<Voluntario, String > clmnNacionalidad;
    @FXML private TableColumn<Sede,Long> clmnId_Sede;
    @FXML private TableColumn<Sede,String> clmnLoc_Sede;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button updateButton;
    @FXML private Button clearButton;
    @FXML private Button returnButton;


// Colecciones

    private ObservableList<Sede> listaSedes;
    private ObservableList<Voluntario> listaVoluntarios;


    /**
     * Función que retorna la lista de sedes
     **/

    public ObservableList<Sede> getSedes() throws JPAException {

        EntityManager em= JPAUtil.getEmf().createEntityManager();
        try{
            List<Sede> milistasedes=em.createQuery( "SELECT e FROM Sede e" ).getResultList();
            listaSedes= FXCollections.observableArrayList( milistasedes );

            return listaSedes;}
        catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }
    }


    /**
     * Función que inicializa el estado de la interfaz de Gestión de Voluntarios
     **/

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            sedeComboBox.setItems( this.getSedes() );
        } catch (JPAException e) {
            e.printStackTrace();
        }
        try {
            VoluntarioTableView.setItems( this.getAll() );
        } catch (JPAException e) {
            e.printStackTrace();
        }
        clmnId_Voluntario.setCellValueFactory( new PropertyValueFactory<Voluntario,Long>("idVoluntario"  ) );
        clmnNombre.setCellValueFactory( new PropertyValueFactory<Voluntario,String>("nombre"  ) );
        clmnApellido1.setCellValueFactory( new PropertyValueFactory<Voluntario,String>("primerApellido" ) );
        clmnApellido2.setCellValueFactory( new PropertyValueFactory<Voluntario,String>("segundoApellido" ) );
        clmnDomicilio.setCellValueFactory( new PropertyValueFactory<Voluntario,String>("domicilio" ) );
        clmnTelefono.setCellValueFactory( new PropertyValueFactory<Voluntario,String>( "telefono" ) );
        clmnEmail.setCellValueFactory( new PropertyValueFactory<Voluntario,String>("email" ) );
        clmnNacionalidad.setCellValueFactory( new PropertyValueFactory<Voluntario,String>("nacionalidad"  ) );
        txtIdVoluntario.setDisable(true);
        gestionarEventos();
    }


    /**
     * Función que permite mostrar en los textfield las propiedades de cada entidad marcada en cada fila de la
     * Tableview de Voluntarios
     **/

    public void gestionarEventos() {

        VoluntarioTableView.getSelectionModel().selectedItemProperty().addListener( new ChangeListener< Voluntario >() {
            @Override
            public void changed(ObservableValue< ? extends Voluntario > observableValue, Voluntario voluntarioAnterior
                    , Voluntario voluntarioSelected) {
                if(voluntarioSelected!=null) {
                    txtIdVoluntario.setText( String.valueOf( voluntarioSelected.getIdVoluntario() ) );
                    txtNombre.setText( voluntarioSelected.getNombre() );
                    txtApellido1.setText( voluntarioSelected.getPrimerApellido() );
                    txtApellido2.setText( voluntarioSelected.getSegundoApellido() );
                    txtDomicilio.setText( voluntarioSelected.getDomicilio() );
                    txtTelefono.setText( voluntarioSelected.getTelefono() );
                    txtEmail.setText( voluntarioSelected.getEmail() );
                    txtNacionalidad.setText( voluntarioSelected.getNacionalidad());
                    sedeComboBox.setValue( voluntarioSelected.getDelegacion() );
                    resultConsole.setText("Para eliminar o modificar registro manten la selección de la linea");

                    addButton.setDisable( true );
                    deleteButton.setDisable( false );
                    updateButton.setDisable( false );
                }

            }
        } );
    }



    /**
     * Función que limpia los textfield y deja todo en el estado inicial
     *
     **/

    @FXML
    public void clearItems(){
        txtNombre.setText( null );
        txtApellido1.setText( null );
        txtApellido2.setText( null );
        txtDomicilio.setText( null );
        txtTelefono.setText( null );
        txtEmail.setText( null );
        txtNacionalidad.setText( null);
        sedeComboBox.setValue( null );
        resultConsole.setText(null);

        addButton.setDisable( false );
        deleteButton.setDisable( true);
        updateButton.setDisable( true );
    }



    /**
     * Función que abre una nueva ventana desde el fichero controller Voluntario hacia el controller del Usuario
     *
     **/


    public Stage OpenWindow(Voluntario voluntario) throws IOException {

        URL url = new File("src/main/java/uoc/entreculturas/view/UsuarioView.fxml").toURI().toURL();
        FXMLLoader fxmlLoader=new FXMLLoader(url);
        Stage stage=new Stage(StageStyle.DECORATED);
        stage.setScene(new Scene((Pane)fxmlLoader.load()));

        UsuarioController controller=fxmlLoader.<UsuarioController>getController();
        controller.setVoluntarioToClase(voluntario);
        stage.showAndWait();
        return stage;
    }


    /**
     * Función que retorne a ventana menu Admin
     *
     **/

    @FXML
    public void returnWindow(ActionEvent event){

        resultConsole.setText("Pendiente implementar esta función");
    }

    /*===================================================
                     OPERACIONES CRUD
    =====================================================*/

    @FXML
    public void save(javafx.event.ActionEvent event) throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();

        Voluntario voluntario=new Voluntario(  );
        voluntario.setNombre( txtNombre.getText() );
        voluntario.setPrimerApellido( txtApellido1.getText() );
        voluntario.setSegundoApellido( txtApellido2.getText() );
        voluntario.setDomicilio( txtDomicilio.getText() );
        voluntario.setTelefono( txtTelefono.getText() );
        voluntario.setEmail( txtEmail.getText() );
        voluntario.setNacionalidad( txtNacionalidad.getText());
        voluntario.setDelegacion( sedeComboBox.getSelectionModel().getSelectedItem() );
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            em.persist( voluntario );
            em.getTransaction().commit();

            em.getTransaction().begin();
            voluntario.setIdVoluntario(findIdVoluntario(voluntario));
            em.merge(voluntario);
            em.getTransaction().commit();
            OpenWindow(voluntario);
            resultConsole.setText( "Se incluyó un registro Voluntario ..... "+
                    "\nSe incluyó un registro usuario" );
            listaVoluntarios.add(voluntario);

        }catch(Exception ex){
            em.getTransaction().rollback();
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
    }

    @FXML
    public void update(javafx.event.ActionEvent event) throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();
        Voluntario mivoluntario=VoluntarioTableView.getSelectionModel().getSelectedItem();

        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            mivoluntario=em.merge(mivoluntario);

            mivoluntario.setNombre( txtNombre.getText() );
            mivoluntario.setPrimerApellido( txtApellido1.getText() );
            mivoluntario.setSegundoApellido( txtApellido2.getText() );
            mivoluntario.setDomicilio( txtDomicilio.getText() );
            mivoluntario.setTelefono( txtTelefono.getText() );
            mivoluntario.setEmail( txtEmail.getText() );
            mivoluntario.setNacionalidad( txtNacionalidad.getText() );
            mivoluntario.setDelegacion( sedeComboBox.getSelectionModel().getSelectedItem() );
            em.merge(mivoluntario);
            em.getTransaction().commit();
            VoluntarioTableView.setItems(getAll());
            resultConsole.setText( "Se actualizó un registro Voluntario ..... " );

        }catch (Exception ex){
            em.getTransaction().rollback();
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
    }

    @FXML
    public void delete(ActionEvent event) throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();
        Voluntario mivoluntario=VoluntarioTableView.getSelectionModel().getSelectedItem();
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }

            em.createQuery("delete from Persona where idPersona= :id").setParameter("id",mivoluntario.getIdPersona()).executeUpdate();

            em.getTransaction().commit();

            resultConsole.setText( "Se eliminó un registro Voluntario ..... " );
            listaVoluntarios.remove( VoluntarioTableView.getSelectionModel().getSelectedIndex());
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }

    }

    public ObservableList<Voluntario > getAll() throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();

        try {
            List<Voluntario> milistaVoluntarios=em.createQuery( "SELECT t FROM Voluntario t").getResultList();
            listaVoluntarios= FXCollections.observableArrayList( milistaVoluntarios );
            return listaVoluntarios;
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }

    }



    /**
     * Función que retorna el idColaborador que genera la tabla de la base de datos
     * @param voluntario
     **/


    public Long findIdVoluntario(Voluntario voluntario) throws JPAException {

        final String GETVOLUNTBYIDPERSON="SELECT v from Voluntario v where v.idPersona=:idpersona";
        Long idpersona=voluntario.getIdPersona();
        Long result;
        EntityManager em=JPAUtil.getEmf().createEntityManager();
        try{
            if(!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            TypedQuery<Voluntario> mivoluntario=em.createQuery(GETVOLUNTBYIDPERSON,Voluntario.class);
            mivoluntario.setParameter("idpersona",idpersona);
            Voluntario mivoluntarioSingleResult=(Voluntario) mivoluntario.getSingleResult();
            em.getTransaction().commit();
            result=mivoluntarioSingleResult.getIdVoluntario();
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
        return result;
    }

    public void goHome(ActionEvent event) throws IOException {
        NavigationService.goHome(event);
    }
}
