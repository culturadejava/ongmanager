package uoc.entreculturas.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.model.Colaborador;
import uoc.entreculturas.model.Sede;
import uoc.entreculturas.service.NavigationService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Creado por @autor: PabloGarcíaNúñez el 15/05/2020
 **/
public class ColaboradorController implements Initializable {

    @FXML
    private TextField txtNombre;
    @FXML private TextField txtApellido1;
    @FXML private TextField txtApellido2;
    @FXML private TextField txtDomicilio;
    @FXML private TextField txtTelefono;
    @FXML private TextField txtEmail;
    @FXML private ComboBox< Sede > sedeComboBox;
    @FXML private TextField txtIdColaborador;
    @FXML private TextArea resultConsole;
    @FXML private TableView< Colaborador > colaboradorTableView;
    @FXML private TableColumn<Colaborador,Long> clmnId_Colaborador;
    @FXML private TableColumn<Colaborador,String> clmnNombre;
    @FXML private TableColumn<Colaborador,String> clmnApellido1;
    @FXML private TableColumn<Colaborador,String> clmnApellido2;
    @FXML private TableColumn<Colaborador,String> clmnDomicilio;
    @FXML private TableColumn<Colaborador,String> clmnTelefono;
    @FXML private TableColumn<Colaborador,String> clmnEmail;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button updateButton;
    @FXML private Button clearButton;
    @FXML private Button returnButton;


// Colecciones

    private ObservableList<Sede> listaSedes;
    private ObservableList<Colaborador> listaColaboradores;


    /**
     * Función que retorna la lista de sedes
     **/
    public ObservableList<Sede> getSedes() throws JPAException {

        EntityManager em= JPAUtil.getEmf().createEntityManager();

        try{
            List<Sede> milistasedes=em.createQuery( "SELECT e FROM Sede e" ).getResultList();
            listaSedes= FXCollections.observableArrayList( milistasedes );

            return listaSedes;}
        catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }

    }

    /**
     * Función que inicializa el estado de la interfaz de Gestión de Colaboradores
     **/


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try{
            sedeComboBox.setItems( this.getSedes() );

        }catch (JPAException e) {
            e.printStackTrace();
        }

        try {
            colaboradorTableView.setItems( this.getAll() );
        } catch (JPAException e) {
            e.printStackTrace();
        }
        clmnId_Colaborador.setCellValueFactory( new PropertyValueFactory<Colaborador,Long>("idColaborador"  ) );
        clmnNombre.setCellValueFactory( new PropertyValueFactory<Colaborador,String>("nombre"  ) );
        clmnApellido1.setCellValueFactory( new PropertyValueFactory<Colaborador,String>("primerApellido" ) );
        clmnApellido2.setCellValueFactory( new PropertyValueFactory<Colaborador,String>("segundoApellido" ) );
        clmnDomicilio.setCellValueFactory( new PropertyValueFactory<Colaborador,String>("domicilio" ) );
        clmnTelefono.setCellValueFactory( new PropertyValueFactory<Colaborador,String>( "telefono" ) );
        clmnEmail.setCellValueFactory( new PropertyValueFactory<Colaborador,String>("email" ) );
        txtIdColaborador.setDisable(true);
        gestionarEventos();
    }


    /**
     * Función que permite mostrar en los textfield las propiedades de cada entidad marcada en cada fila de la
     * Tableview de Colaboradores
     **/



    public void gestionarEventos() {

        colaboradorTableView.getSelectionModel().selectedItemProperty().addListener( new ChangeListener< Colaborador >() {
            @Override
            public void changed(ObservableValue< ? extends Colaborador > observableValue, Colaborador colaboradorAnterior
                    , Colaborador colaboradorSelected) {
                if(colaboradorSelected!=null) {
                    txtIdColaborador.setText( String.valueOf( colaboradorSelected.getIdColaborador() ) );
                    txtNombre.setText( colaboradorSelected.getNombre() );
                    txtApellido1.setText( colaboradorSelected.getPrimerApellido() );
                    txtApellido2.setText( colaboradorSelected.getSegundoApellido() );
                    txtDomicilio.setText( colaboradorSelected.getDomicilio() );
                    txtTelefono.setText( colaboradorSelected.getTelefono() );
                    txtEmail.setText( colaboradorSelected.getEmail() );
                    sedeComboBox.setValue( colaboradorSelected.getDelegacion() );
                    resultConsole.setText("Para eliminar o modificar registro manten la selección de la linea");

                    addButton.setDisable( true );
                    deleteButton.setDisable( false );
                    updateButton.setDisable( false );
                }

            }
        } );
    }

    /**
     * Función que limpia los textfield y deja todo en el estado inicial
     *
     **/

    @FXML
    public void clearItems(){
        txtNombre.setText( null );
        txtApellido1.setText( null );
        txtApellido2.setText( null );
        txtDomicilio.setText( null );
        txtTelefono.setText( null );
        txtEmail.setText( null );
        sedeComboBox.setValue( null );
        resultConsole.setText(null);

        addButton.setDisable(false);
        deleteButton.setDisable(true);
        updateButton.setDisable(true);

    }


    /**
     * Función que abre una nueva ventana desde el fichero controller Colaborador hacia el controller del Usuario
     *
     **/


    public Stage OpenWindow(Colaborador colaborador) throws IOException, JPAException {

        URL url = new File("src/main/java/uoc/entreculturas/view/UsuarioView.fxml").toURI().toURL();
        FXMLLoader fxmlLoader=new FXMLLoader(url);
        Stage stage=new Stage(StageStyle.DECORATED);
        stage.setScene(new Scene((Pane)fxmlLoader.load()));

        UsuarioController controller=fxmlLoader.<UsuarioController>getController();
        controller.setColaboradorToClase(colaborador);
        stage.showAndWait();
        return stage;
    }



    /**
     * Función que retorne a ventana menu Admin
     *
     **/

    @FXML
    public void returnWindow(ActionEvent event){

        resultConsole.setText("Pendiente implementar esta función");
    }

    /*===================================================
                     OPERACIONES CRUD
    =====================================================*/

    @FXML
    public void save(ActionEvent event) throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();

        Colaborador colaborador=new Colaborador(  );
        colaborador.setNombre( txtNombre.getText() );
        colaborador.setPrimerApellido( txtApellido1.getText() );
        colaborador.setSegundoApellido( txtApellido2.getText() );
        colaborador.setDomicilio( txtDomicilio.getText() );
        colaborador.setTelefono( txtTelefono.getText() );
        colaborador.setEmail( txtEmail.getText() );
        colaborador.setDelegacion( sedeComboBox.getSelectionModel().getSelectedItem() );
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            em.persist( colaborador );
            em.getTransaction().commit();

            em.getTransaction().begin();
            colaborador.setIdColaborador(findIdColaborador(colaborador));
            em.merge(colaborador);
            em.getTransaction().commit();
            OpenWindow(colaborador);
            resultConsole.setText( "Se incluyó un registro Colaborador ..... "+
                    "\nSe incluyó un registro usuario" );
            listaColaboradores.add(colaborador);
        }catch(Exception ex){
            em.getTransaction().rollback();
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
    }

    @FXML
    public void update(javafx.event.ActionEvent event) throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();
        Colaborador micolaborador=colaboradorTableView.getSelectionModel().getSelectedItem();

        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            micolaborador=em.merge(micolaborador);

            micolaborador.setNombre( txtNombre.getText() );
            micolaborador.setPrimerApellido( txtApellido1.getText() );
            micolaborador.setSegundoApellido( txtApellido2.getText() );
            micolaborador.setDomicilio( txtDomicilio.getText() );
            micolaborador.setTelefono( txtTelefono.getText() );
            micolaborador.setEmail( txtEmail.getText() );
            micolaborador.setDelegacion( sedeComboBox.getSelectionModel().getSelectedItem() );
            em.merge(micolaborador);
            em.getTransaction().commit();
            colaboradorTableView.setItems(getAll());
            resultConsole.setText( "Se actualizó un registro Colaborador ..... " );

        }catch (Exception ex){
            em.getTransaction().rollback();
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
    }

    @FXML
    public void delete(ActionEvent event) throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();
        Colaborador micolaborador=colaboradorTableView.getSelectionModel().getSelectedItem();
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }

            em.createQuery("delete from Persona where idPersona= :id").setParameter("id",micolaborador.getIdPersona()).executeUpdate();

            em.getTransaction().commit();

            resultConsole.setText( "Se eliminó un registro Colaborador ..... " );
            listaColaboradores.remove( colaboradorTableView.getSelectionModel().getSelectedIndex());
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }

    }


    public ObservableList<Colaborador > getAll() throws JPAException {

        EntityManager em=JPAUtil.getEmf().createEntityManager();

        try {
            List<Colaborador> milistaColaboradores=em.createQuery( "SELECT t FROM Colaborador t").getResultList();
            listaColaboradores= FXCollections.observableArrayList( milistaColaboradores );
            return listaColaboradores;
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }
    }

    /**
     * Función que retorna el idColaborador que genera la tabla de la base de datos
     * @param colaborador
     **/


    public Long findIdColaborador(Colaborador colaborador) throws JPAException {

        final String GETCOLABBYIDPERSON="SELECT c from Colaborador c where c.idPersona=:idpersona";
        Long idpersona=colaborador.getIdPersona();
        Long result;
        EntityManager em=JPAUtil.getEmf().createEntityManager();
        try{
            if(!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            TypedQuery<Colaborador> micolaborador=em.createQuery(GETCOLABBYIDPERSON,Colaborador.class);
            micolaborador.setParameter("idpersona",idpersona);
            Colaborador micolaboradorSingleResult=(Colaborador)micolaborador.getSingleResult();
            em.getTransaction().commit();
            result=micolaboradorSingleResult.getIdColaborador();
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
       return result;
    }

    public void goHome(ActionEvent event) throws IOException {
        NavigationService.goHome(event);
    }
}
