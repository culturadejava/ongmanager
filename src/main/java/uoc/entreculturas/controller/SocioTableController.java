package uoc.entreculturas.controller;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.dao.DaoFactory;
import uoc.entreculturas.dao.sql.SQLSocioDao;
import uoc.entreculturas.model.*;
import uoc.entreculturas.service.NavigationService;


import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class SocioTableController implements Initializable{

    static SQLSocioDao socioDao;

    static {
        try {
            socioDao = (SQLSocioDao) DaoFactory.getDaoFactory("SQL").getDao("Socio");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public SocioTableController() throws SQLException {
    }

    public class Item {

        public SimpleLongProperty socioId = new SimpleLongProperty();
        public SimpleStringProperty nombre = new SimpleStringProperty();
        public SimpleStringProperty primerApellido = new SimpleStringProperty();
        public SimpleStringProperty segundoApellido = new SimpleStringProperty();
        public SimpleStringProperty domicilio = new SimpleStringProperty();
        public SimpleStringProperty telefono = new SimpleStringProperty();
        public SimpleStringProperty email = new SimpleStringProperty();
        //public SimpleFloatProperty importe = new SimpleFloatProperty();


        public Long getSocioId() {
            return socioId.get();
        }

        public String getNombre() {
            return nombre.get();
        }

        public String getPrimerApellido() {
            return primerApellido.get();
        }

        public String getSegundoApellido() {
            return segundoApellido.get();
        }

        public String getDomicilio() {
            return domicilio.get();
        }

        public String getTelefono() {
            return telefono.get();
        }

        public String getEmail() {
            return email.get();
        }

        /*public float getImporte() {
            return importe.get();
        }*/


    }

    @FXML
    TableView<SocioTableController.Item> itemTbl;

    @FXML
    TableColumn itemSocioId;
    @FXML
    TableColumn itemNombre;
    @FXML
    TableColumn itemPrimerApellido;
    @FXML
    TableColumn itemSegundoApellido;
    @FXML
    TableColumn itemDomicilio;
    @FXML
    TableColumn itemTelefono;
    @FXML
    TableColumn itemEmail;
    @FXML
    TableColumn itemEdit;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        // Set up the table data
        itemSocioId.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, Long>("socioId")
        );
        itemNombre.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, String>("nombre")
        );
        itemPrimerApellido.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, String>("primerApellido")
        );
        itemSegundoApellido.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, String>("segundoApellido")
        );
        itemDomicilio.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, String>("domicilio")
        );
        itemTelefono.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, String>("telefono")
        );

        itemEmail.setCellValueFactory(
                new PropertyValueFactory<SocioTableController.Item, String>("email")
        );

        addButtonToTable();

        ObservableList<SocioTableController.Item> data = FXCollections.observableArrayList();

        try {
            for (Socio socio : this.socioDao.getAll()) {
                SocioTableController.Item item = new SocioTableController.Item();
                item.socioId.setValue(socio.getSocioId());
                item.nombre.setValue(socio.getNombre());
                item.primerApellido.setValue(socio.getPrimerApellido());
                item.segundoApellido.setValue(socio.getSegundoApellido());
                item.domicilio.setValue(socio.getDomicilio());
                item.telefono.setValue(socio.getTelefono());
                item.email.setValue(socio.getEmail());
                data.add(item);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        itemTbl.setItems(data);
    }

    public void addButtonToTable() {
        Callback<TableColumn<SocioTableController.Item, Void>, TableCell<SocioTableController.Item, Void>> cellFactory =
                new Callback<TableColumn<SocioTableController.Item, Void>, TableCell<SocioTableController.Item, Void>>() {
                    @Override
                    public TableCell<SocioTableController.Item, Void> call(final TableColumn<SocioTableController.Item, Void> param) {
                        final TableCell<SocioTableController.Item, Void> cell = new TableCell<SocioTableController.Item, Void>() {

                            private final Button btn = new Button("Action");

                            {
                                btn.setOnAction((ActionEvent event) -> {
                                    SocioTableController.Item data = getTableView().getItems().get(getIndex());
                                    try {
                                        changeScreenButtonPushed(event, data.getSocioId());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (DaoException e) {
                                        e.printStackTrace();
                                    } catch (JAXBException e) {
                                        e.printStackTrace();
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                });
                            }

                            @Override
                            public void updateItem(Void item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                } else {
                                    setGraphic(btn);
                                }
                            }
                        };
                        return cell;
                    }
                };

        itemEdit.setCellFactory(cellFactory);
    }

    /**
     * When this method is called, it will change the Scene to
     * a TableView example
     */
    public void changeScreenButtonPushed(ActionEvent event, Long socioId) throws IOException, DaoException, JAXBException, SQLException {
        URL url = new File("src/main/java/uoc/entreculturas/view/SocioView.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent SocioView = loader.load();

        SocioController controller = loader.getController();
        controller.setSocio(socioId);

        Scene tableViewScene = new Scene(SocioView);

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void goHome(ActionEvent event) throws IOException {
        NavigationService.goHome(event);
    }
}
