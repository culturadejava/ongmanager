package uoc.entreculturas.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.model.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Creado por @autor: PabloGarcíaNúñez el 15/05/2020
 **/
public class UsuarioController implements Initializable {

    @FXML private TextField txtNick;
    @FXML private TextField txtPassword;
    @FXML private TextField txtcorpEmail;
    @FXML private Button saveButton;
    @FXML private Button closeButtom;
    @FXML private TextArea resultArea;
    @FXML private Label profileLabel;

    private Trabajador trabajador;
    private Colaborador colaborador;
    private Voluntario voluntario;
    private Usuario usuario;

    // Colecciones

    private ObservableList<Usuario> listaUsuarios;


    public UsuarioController(){ }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        resultArea.setText("Rellena todos los campos");
        saveButton.setDisable(true);
        closeButtom.setDisable(true);
    }

    public ObservableList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    /**
     * Método que devuelve true si los parámetros de Usuario nick y password coincide con
     * los de otro objeto de la lista
     * @param
     * @return
     */
    public Boolean EsUsuarioDuplicado(Usuario usuarioToCompare) throws JPAException {

        final String GETDUPLICADO = "select u from Usuario u WHERE u.nick=:username and u.password=:password";
        EntityManager em = JPAUtil.getEmf().createEntityManager();
        String username = usuarioToCompare.getNick();
        String password = usuarioToCompare.getPassword();
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            TypedQuery<Usuario> miusuario = em.createQuery(GETDUPLICADO, Usuario.class);
            miusuario.setParameter("username", username);
            miusuario.setParameter("password", password);
            Usuario usuario = (Usuario) miusuario.getSingleResult();
            em.getTransaction().commit();

         }catch(NoResultException exception){
            return false;
        }
        catch(Exception exception){
            throw new JPAException(exception.getMessage(),exception);
            }finally {
            em.close();
        }
            return true;
    }

    @FXML
    public void pruebainterna(ActionEvent event) {

        Usuario miusuario = new Usuario(txtNick.getText(), txtcorpEmail.getText(), txtPassword.getText());
        try {
            if (EsUsuarioDuplicado(miusuario)) {
                resultArea.setText("Este usuario ya existe" +
                        "Introduce otros datos");
            } else
                resultArea.setText("El usuario es válido");
        } catch (JPAException e) {
            e.printStackTrace();
        }
    }

        /**
     * Método que devuelve borra todos los txtfield de la pantalla Usuario y desactiva el boton de guardar
     *
     */


    public void resetUsuario(){

        txtNick.setText(null);
        txtPassword.setText(null);
        txtcorpEmail.setText(null);

    }

    @FXML
    public void WindowClose(ActionEvent event)  {
        profileLabel.setText(null);
        Stage stage=(Stage) closeButtom.getScene().getWindow();
        stage.close();
    }



    @FXML
    public void enablesaveButtom(){
        String username=txtNick.getText();
        String password=txtPassword.getText();
        String email=txtcorpEmail.getText();
        boolean isDisable= (username.isEmpty() || username.trim().isEmpty()) || (password.isEmpty() || password.trim().isEmpty()) || (email.isEmpty() || email.trim().isEmpty());
        saveButton.setDisable(isDisable);
    }


    /*===================================================
                     OPERACIONES CRUD
    =====================================================*/

    @FXML
    public void save(ActionEvent event) throws JPAException {

        String mensaje="Se registró el Usuario";
        EntityManager em=JPAUtil.getEmf().createEntityManager();
        Usuario miusuario = new Usuario(txtNick.getText(), txtcorpEmail.getText(), txtPassword.getText());
        try{
            if (EsUsuarioDuplicado(miusuario)) {
                resultArea.setText("Este usuario ya existe"+
                        "\nIntroduce otros datos");
                resetUsuario();
            }else{
                if (!em.getTransaction().isActive()){
                    em.getTransaction().begin();
                }

                if(profileLabel.getText().equals("Trabajador")) {
                    this.trabajador = em.merge(this.trabajador);
                    miusuario.setPersona(this.trabajador);
                    resultArea.setText(miusuario.getPersona().toString());
                }
                if(profileLabel.getText().equals("Colaborador")) {
                    this.colaborador = em.merge(this.colaborador);
                    miusuario.setPersona((this.colaborador));
                }
                if(profileLabel.getText().equals("Voluntario")) {
                    this.voluntario = em.merge(this.voluntario);
                    miusuario.setPersona(this.voluntario);
                }

                em.persist( miusuario );
                em.getTransaction().commit();
                resultArea.setText(mensaje);

                closeButtom.setDisable(false);
                resultArea.setText(mensaje+"\nPulsa boton cerrar");
                saveButton.setDisable(true);
            }


        }catch(Exception ex){
            em.getTransaction().rollback();
            throw new JPAException(ex.getMessage(),ex);
        }


    }

    public void setTrabajadorToClase(Trabajador trabajador)  {

        this.trabajador=trabajador;
        this.profileLabel.setText(trabajador.getClass().getSimpleName());


    }

    public void setColaboradorToClase(Colaborador colaborador){
        this.colaborador=colaborador;
        this.profileLabel.setText(colaborador.getClass().getSimpleName());

    }

    public void setVoluntarioToClase(Voluntario voluntario){
        this.voluntario=voluntario;
        this.profileLabel.setText(voluntario.getClass().getSimpleName());

    }


    public ObservableList<Usuario> getAll() throws JPAException {


        EntityManager em=JPAUtil.getEmf().createEntityManager();

        try {
            List<Usuario> milistaUsuarios=em.createQuery( "SELECT u FROM Usuario u").getResultList();
            listaUsuarios= FXCollections.observableArrayList( milistaUsuarios );
            return listaUsuarios;
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }


    }
}
