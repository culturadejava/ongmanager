package uoc.entreculturas.controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.service.NavigationService;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class AdminMenuController implements Initializable {

    /**
     * When this method is called, it will change the Scene to
     * a TableView example
     */
    public void openSedes(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/SedesTableView.fxml", event);
    }

    public void openTrabajadores(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/TrabajadorView.fxml", event);
    }

    public void openVoluntarios(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/VoluntarioView.fxml", event);
    }

    public void openColaboradores(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/ColaboradorView.fxml", event);
    }

    public void openProyectos(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/ProyectoTableView.fxml", event);
    }

    public void openSocios(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/SocioTableView.fxml", event);
    }

    public void logout(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/LoginView.fxml", event);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { }
}
