package uoc.entreculturas.controller;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.model.Colaborador;
import uoc.entreculturas.model.Sede;
import uoc.entreculturas.model.Trabajador;
import uoc.entreculturas.service.NavigationService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;



/**
 * Creado por @autor:
 **/

public class TrabajadorController implements Initializable {

    @FXML private TextField txtNombre;
    @FXML private TextField txtApellido1;
    @FXML private TextField txtApellido2;
    @FXML private TextField txtDomicilio;
    @FXML private TextField txtTelefono;
    @FXML private TextField txtEmail;
    @FXML private TextField txtSalario;
    @FXML private ComboBox< Sede > sedeComboBox;
    @FXML private TextField txtIdEmpleado;
    @FXML private TextArea resultConsole;
    @FXML private TableView<Trabajador> trabajadorTableView;
    @FXML private TableColumn<Trabajador,Long> clmnId_Trabajador;
    @FXML private TableColumn<Trabajador,String> clmnNombre;
    @FXML private TableColumn<Trabajador,String> clmnApellido1;
    @FXML private TableColumn<Trabajador,String> clmnApellido2;
    @FXML private TableColumn<Trabajador,String> clmnDomicilio;
    @FXML private TableColumn<Trabajador,String> clmnTelefono;
    @FXML private TableColumn<Trabajador,String> clmnEmail;
    @FXML private TableColumn<Trabajador, Float > clmnSalario;
    @FXML private TableColumn<Sede,Long> clmnId_Sede;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button updateButton;
    @FXML private Button clearButton;
    @FXML private Button returnButton;


// Colecciones

    private ObservableList<Sede> listaSedes;
    private ObservableList<Trabajador> listaTrabajadores;


    /**
     * Función que retorna la lista de sedes
     **/

    public ObservableList<Sede> getSedes() throws JPAException {


        EntityManager em=JPAUtil.getEmf().createEntityManager();
        try{
        List<Sede> milistasedes=em.createQuery( "SELECT e FROM Sede e" ).getResultList();
        listaSedes= FXCollections.observableArrayList( milistasedes );

        return listaSedes;}
        catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }

    }


    /**
     * Función que inicializa el estado de la interfaz de Gestión de Trabajadores
     **/

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            sedeComboBox.setItems( this.getSedes() );
        } catch (JPAException e) {
            e.printStackTrace();
        }
        try {
            trabajadorTableView.setItems( this.getAll() );
        } catch (JPAException e) {
            e.printStackTrace();
        }
        clmnId_Trabajador.setCellValueFactory(new PropertyValueFactory<Trabajador,Long>("idTrabajador") );
        clmnNombre.setCellValueFactory( new PropertyValueFactory<Trabajador,String>("nombre"  ) );
        clmnApellido1.setCellValueFactory( new PropertyValueFactory<Trabajador,String>("primerApellido" ) );
        clmnApellido2.setCellValueFactory( new PropertyValueFactory<Trabajador,String>("segundoApellido" ) );
        clmnDomicilio.setCellValueFactory( new PropertyValueFactory<Trabajador,String>("domicilio" ) );
        clmnTelefono.setCellValueFactory( new PropertyValueFactory<Trabajador,String>( "telefono" ) );
        clmnEmail.setCellValueFactory( new PropertyValueFactory<Trabajador,String>("email" ) );
        clmnSalario.setCellValueFactory( new PropertyValueFactory<Trabajador,Float>("salario" ) );
        txtIdEmpleado.setDisable(true);
        gestionarEventos();
    }


    /**
     * Función que permite mostrar en los textfield las propiedades de cada entidad marcada en cada fila de la
     * Tableview de Trabajadores
     **/

    public void gestionarEventos() {

        trabajadorTableView.getSelectionModel().selectedItemProperty().addListener( new ChangeListener< Trabajador >() {
            @Override
            public void changed(ObservableValue< ? extends Trabajador > observableValue, Trabajador trabajadorAnterior
                    , Trabajador trabajadorSelected) {
                if(trabajadorSelected!=null) {
                    txtIdEmpleado.setText( String.valueOf( trabajadorSelected.getIdTrabajador()) );
                    txtNombre.setText( trabajadorSelected.getNombre() );
                    txtApellido1.setText( trabajadorSelected.getPrimerApellido() );
                    txtApellido2.setText( trabajadorSelected.getSegundoApellido() );
                    txtDomicilio.setText( trabajadorSelected.getDomicilio() );
                    txtTelefono.setText( trabajadorSelected.getTelefono() );
                    txtEmail.setText( trabajadorSelected.getEmail() );
                    txtSalario.setText( String.valueOf( trabajadorSelected.getSalario() ) );
                    sedeComboBox.setValue( trabajadorSelected.getDelegacion() );
                    resultConsole.setText("Para eliminar o modificar registro manten la selección de la linea");

                    addButton.setDisable( true );
                    deleteButton.setDisable( false );
                    updateButton.setDisable( false );
                }

            }
        } );
    }


    /**
     * Función que limpia los textfield y deja todo en el estado inicial
     *
     **/

    @FXML
    public void clearItems(){
        txtNombre.setText( null );
        txtApellido1.setText( null );
        txtApellido2.setText( null );
        txtDomicilio.setText( null );
        txtTelefono.setText( null );
        txtEmail.setText( null );
        txtSalario.setText( null);
        sedeComboBox.setValue( null );
        resultConsole.setText(null);

        addButton.setDisable( false );
        deleteButton.setDisable( true);
        updateButton.setDisable( true );

    }


    /**
     * Función que abre una nueva ventana desde el fichero controller Trabajador hacia el controller del Usuario
     *
     **/


    public Stage OpenWindow(Trabajador trabajador) throws IOException {

        URL url = new File("src/main/java/uoc/entreculturas/view/UsuarioView.fxml").toURI().toURL();
        FXMLLoader fxmlLoader=new FXMLLoader(url);
        Stage stage=new Stage(StageStyle.DECORATED);
        stage.setScene(new Scene((Pane)fxmlLoader.load()));

        UsuarioController controller=fxmlLoader.<UsuarioController>getController();
        controller.setTrabajadorToClase(trabajador);
        stage.showAndWait();
        return stage;
    }


    /**
     * Función que retorne a ventana menu Admin
     *
     **/

    @FXML
    public void returnWindow(ActionEvent event){

        resultConsole.setText("Pendiente implementar esta función");
    }



    /*===================================================
                     OPERACIONES CRUD
    =====================================================*/

     @FXML
     public void save(javafx.event.ActionEvent event) throws JPAException, IOException {

         EntityManager em=JPAUtil.getEmf().createEntityManager();

         Trabajador trabajador=new Trabajador(  );
         trabajador.setNombre( txtNombre.getText() );
         trabajador.setPrimerApellido( txtApellido1.getText() );
         trabajador.setSegundoApellido( txtApellido2.getText() );
         trabajador.setDomicilio( txtDomicilio.getText() );
         trabajador.setTelefono( txtTelefono.getText() );
         trabajador.setEmail( txtEmail.getText() );
         trabajador.setSalario( Float.parseFloat( txtSalario.getText() ) );
         trabajador.setDelegacion( sedeComboBox.getSelectionModel().getSelectedItem() );
         try{
             if (!em.getTransaction().isActive()){
                 em.getTransaction().begin();
                    }
             em.persist( trabajador );
             em.getTransaction().commit();

             em.getTransaction().begin();
             trabajador.setIdTrabajador(findIdTrabajador(trabajador));
             em.merge(trabajador);
             em.getTransaction().commit();
             OpenWindow(trabajador);
             resultConsole.setText( "Se incluyó un registro Trabajador ..... "+
                     "\nSe incluyó un registro usuario" );
             listaTrabajadores.add(trabajador);

         }catch(Exception ex){
                 em.getTransaction().rollback();
                 throw new JPAException(ex.getMessage(),ex);
             }finally {
             em.close();
         }
     }

    @FXML
    public void update(javafx.event.ActionEvent event) throws JPAException {

         EntityManager em=JPAUtil.getEmf().createEntityManager();
         Trabajador mitrabajador=trabajadorTableView.getSelectionModel().getSelectedItem();

        try{
             if (!em.getTransaction().isActive()){
                 em.getTransaction().begin();
             }
             mitrabajador=em.merge(mitrabajador);

             mitrabajador.setNombre( txtNombre.getText() );
             mitrabajador.setPrimerApellido( txtApellido1.getText() );
             mitrabajador.setSegundoApellido( txtApellido2.getText() );
             mitrabajador.setDomicilio( txtDomicilio.getText() );
             mitrabajador.setTelefono( txtTelefono.getText() );
             mitrabajador.setEmail( txtEmail.getText() );
             mitrabajador.setSalario( Float.parseFloat( txtSalario.getText() ) );
             mitrabajador.setDelegacion( sedeComboBox.getSelectionModel().getSelectedItem() );
             em.merge(mitrabajador);
             em.getTransaction().commit();
             trabajadorTableView.setItems(getAll());
             resultConsole.setText( "Se actualizó un registro Trabajador ..... " );

         }catch (Exception ex){
             em.getTransaction().rollback();
             throw new JPAException(ex.getMessage(),ex);
         }finally {
             em.close();
         }
     }

     @FXML
     public void delete(ActionEvent event) throws JPAException {
         EntityManager em=JPAUtil.getEmf().createEntityManager();
         Trabajador mitrabajador=trabajadorTableView.getSelectionModel().getSelectedItem();
         try{
             if (!em.getTransaction().isActive()){
                 em.getTransaction().begin();
             }

             em.createQuery("delete from Persona where idPersona= :id").setParameter("id",mitrabajador.getIdPersona()).executeUpdate();

             em.getTransaction().commit();

             resultConsole.setText( "Se eliminó un registro Trabajador ..... " );
             listaTrabajadores.remove( trabajadorTableView.getSelectionModel().getSelectedIndex());
         }catch (Exception ex){
             throw new JPAException(ex.getMessage(),ex);
            }finally {
             em.close();
         }
     }

     public ObservableList<Trabajador > getAll() throws JPAException {


         EntityManager em=JPAUtil.getEmf().createEntityManager();

         try {
             List<Trabajador> milistaTrabajadores=em.createQuery( "SELECT t FROM Trabajador t").getResultList();
             listaTrabajadores= FXCollections.observableArrayList( milistaTrabajadores );
             return listaTrabajadores;
         }catch (Exception ex){
             throw new JPAException(ex.getMessage(),ex);
         }

     }

    /**
     * Función que retorna el idColaborador que genera la tabla de la base de datos
     * @param trabajador
     **/


    public Long findIdTrabajador(Trabajador trabajador) throws JPAException {

        final String GETTRABBYIDPERSON="SELECT t from Trabajador t where t.idPersona=:idpersona";
        Long idpersona=trabajador.getIdPersona();
        Long result;
        EntityManager em=JPAUtil.getEmf().createEntityManager();
        try{
            if(!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            TypedQuery<Trabajador> mitrabajador=em.createQuery(GETTRABBYIDPERSON,Trabajador.class);
            mitrabajador.setParameter("idpersona",idpersona);
            Trabajador mitrabajadorSingleResult=(Trabajador) mitrabajador.getSingleResult();
            em.getTransaction().commit();
            result=mitrabajadorSingleResult.getIdTrabajador();
        }catch (Exception ex){
            throw new JPAException(ex.getMessage(),ex);
        }finally {
            em.close();
        }
        return result;
    }

    public void goHome(ActionEvent event) throws IOException {
        NavigationService.goHome(event);
    }
}


