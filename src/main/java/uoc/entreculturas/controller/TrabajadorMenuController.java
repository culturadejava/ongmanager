package uoc.entreculturas.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.JPAUtil;
import uoc.entreculturas.model.Trabajador;
import uoc.entreculturas.model.Usuario;

import javax.persistence.EntityManager;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Creado por @autor: PabloGarcíaNúñez el 15/05/2020
 **/
public class TrabajadorMenuController implements Initializable {


    @FXML private TextField editNickname;
    @FXML private TextField editPassword;
    @FXML private TextField editEmail;

    @FXML private TextField txtNombre;
    @FXML private TextField txtApellido1;
    @FXML private TextField txtApellido2;
    @FXML private TextField txtDomicilio;
    @FXML private TextField txtSede;
    @FXML private TextField txtNick;
    @FXML private TextField txtPass;
    @FXML private TextField txtEmail;

    @FXML private TextArea resultArea;

    @FXML Button updateButton;
    @FXML Button ExitButton;

    private Trabajador trabajador;
    private UsuarioController usuarioController;

    //colecciones

    private ObservableList<Usuario> listaUsuarios;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        resultArea.setText(null);
        editNickname.setText(null);
        editPassword.setText(null);
        editEmail.setText(null);
        updateButton.setDisable(true);

    }


    public void clearItems(){
        editNickname.setText(null);
        editPassword.setText(null);
        editEmail.setText(null);
    }


    @FXML
    public void enableUpdateButtom(){
        String username=editNickname.getText();
        String password=editPassword.getText();
        String email=editEmail.getText();
        boolean isDisable= (username.isEmpty() || username.trim().isEmpty()) || (password.isEmpty() || password.trim().isEmpty()) || (email.isEmpty() || email.trim().isEmpty());
        updateButton.setDisable(isDisable);
    }

    public void updateUserData(ActionEvent event) throws JPAException {

        EntityManager em= JPAUtil.getEmf().createEntityManager();
        Usuario miusuario=this.trabajador.getUsuario();
        this.usuarioController=new UsuarioController();
        listaUsuarios=this.usuarioController.getAll();
        try{
            if (!em.getTransaction().isActive()){
                em.getTransaction().begin();
            }
            miusuario=em.merge(miusuario);

            miusuario.setNick(editNickname.getText());
            miusuario.setPassword(editPassword.getText());
            miusuario.setEmail(editEmail.getText());
            if (this.usuarioController.EsUsuarioDuplicado(miusuario)){
                resultArea.setText("El usuario/password está duplicado."+"\nIntroduce otros datos");
                clearItems();

            }else {
                this.trabajador.setUsuario(miusuario);
                em.merge(this.trabajador);
                em.getTransaction().commit();
                txtNick.setText(miusuario.getNick());
                txtPass.setText(miusuario.getPassword());
                txtEmail.setText(miusuario.getEmail());
                resultArea.setText("Se actualizarón los datos de Usuario del Trabajador");
            }
        } catch (Exception e) {
            throw new JPAException(e.getMessage(),e);
        }finally {
            em.close();
        }
    }



    public void setTrabajadorDataToMenu(Trabajador trabajador){

        txtNombre.setText(trabajador.getNombre());
        txtApellido1.setText(trabajador.getPrimerApellido());
        txtApellido2.setText(trabajador.getSegundoApellido());
        txtDomicilio.setText(trabajador.getDomicilio());
        txtSede.setText(trabajador.getDelegacion().getLocation());
        txtNick.setText(trabajador.getUsuario().getNick());
        txtPass.setText(trabajador.getUsuario().getPassword());
        txtEmail.setText(trabajador.getUsuario().getEmail());
        this.trabajador=trabajador;


    }


    @FXML
    public void WindowClose(ActionEvent event)  {
        JPAUtil.shutdown();
        Stage stage=(Stage) ExitButton.getScene().getWindow();
        stage.close();
    }



}
