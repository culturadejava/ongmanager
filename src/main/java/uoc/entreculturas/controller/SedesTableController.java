package uoc.entreculturas.controller;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import uoc.entreculturas.dao.*;
import uoc.entreculturas.dao.sql.*;
import uoc.entreculturas.model.Sede;
import uoc.entreculturas.service.NavigationService;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class SedesTableController implements Initializable {
    static private SQLSedeDAO sedesDao;

    static {
        try {
            sedesDao = (SQLSedeDAO) DaoFactory.getDaoFactory("SQL").getDao("Sede");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ObservableList<Sede> data = FXCollections.observableArrayList();

    @FXML
    TextField locationInput = new TextField();
    @FXML
    CheckBox isCentralInput = new CheckBox();

    @FXML
    TableView<Sede> itemTbl;

    @FXML
    TableColumn itemId;
    @FXML
    TableColumn itemLocation;
    @FXML
    TableColumn itemCentral;
    @FXML
    TableColumn itemEdit;


    public SedesTableController() throws SQLException, JAXBException, DaoException {

    }

    public void clearForm() {
        this.locationInput.clear();
        this.isCentralInput.setSelected(false);
    }

    public void addCustomSedeOnClick(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        if (locationInput.getText().isEmpty() == false) {
            Sede sede = new Sede(isCentralInput.isSelected(), locationInput.getText());

            sedesDao.save(sede);

            updateSedes();

            clearForm();
        }
    }

    public class Item {
        public SimpleLongProperty id = new SimpleLongProperty();
        public SimpleStringProperty location = new SimpleStringProperty();
        public SimpleBooleanProperty central = new SimpleBooleanProperty();

        public Long getId() {
            return id.get();
        }

        public String getLocation() {
            return location.get();
        }

        public Boolean getCentral() {
            return central.get();
        }
    }

    public void addButtonToTable() {
        Callback<TableColumn<Item, Void>, TableCell<Item, Void>> cellFactory =
                new Callback<TableColumn<Item, Void>, TableCell<Item, Void>>() {
                    @Override
                    public TableCell<Item, Void> call(final TableColumn<Item, Void> param) {
                        final TableCell<Item, Void> cell = new TableCell<Item, Void>() {

                            private final Button btn = new Button("Ver");

                            {
                                btn.setOnAction((ActionEvent event) -> {
                                    Long id = data.get(getIndex()).getId();
                                    try {
                                        changeScreenButtonPushed(event, id);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (DaoException e) {
                                        e.printStackTrace();
                                    } catch (JAXBException e) {
                                        e.printStackTrace();
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                });
                            }

                            @Override
                            public void updateItem(Void item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                } else {
                                    setGraphic(btn);
                                }
                            }
                        };
                        return cell;
                    }
                };

        itemEdit.setCellFactory(cellFactory);
    }

    /**
     * When this method is called, it will change the Scene to
     * a TableView example
     */
    public void changeScreenButtonPushed(ActionEvent event, Long id) throws IOException, DaoException, JAXBException, SQLException {
        URL url = new File("src/main/java/uoc/entreculturas/view/SedeView.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent sedeView = loader.load();

        SedeController controller = loader.getController();
        controller.setSede(id);

        Scene tableViewScene = new Scene(sedeView);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    public void goHome(ActionEvent event) throws IOException {
        NavigationService.goHome(event);
    }

    public void updateSedes() {
        data.clear();

        try {
            for (Sede sede : this.sedesDao.getAll()) {
                data.add(sede);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Set up the table data
        itemId.setCellValueFactory(
                new PropertyValueFactory<Sede,Long>("id")
        );
        itemLocation.setCellValueFactory(
                new PropertyValueFactory<Sede,String>("location")
        );
        itemCentral.setCellValueFactory(
                new PropertyValueFactory<Sede,Boolean>("isCentral")
        );

        addButtonToTable();

        updateSedes();

        itemTbl.setItems(data);
    }
}
