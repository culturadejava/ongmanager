package uoc.entreculturas.controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.dao.DaoFactory;
import uoc.entreculturas.dao.sql.SQLSocioDao;
import uoc.entreculturas.model.*;


import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ResourceBundle;


public class SocioController implements Initializable {

    private Socio model;
    static SQLSocioDao socioDao;

    @FXML private TextField socioId, nombre, primerApellido, segundoApellido, domicilio, telefono, email;

    public SocioController() {
        this.socioId = new TextField();
        this.nombre = new TextField();
        this.primerApellido = new TextField();
        this.segundoApellido = new TextField();
        this.domicilio = new TextField();
        this.telefono = new TextField();
        this.email = new TextField();
    }

    public SocioController(Socio model) {

        this.model = model;
        this.socioId = new TextField();
        this.nombre = new TextField();
        this.primerApellido = new TextField();
        this.segundoApellido = new TextField();
        this.domicilio = new TextField();
        this.telefono = new TextField();
        this.email = new TextField();
    }



    public void setSocio(Long socioId) throws DaoException, JAXBException, SQLException {

        SQLSocioDao socioDAO = (SQLSocioDao) DaoFactory.getDaoFactory("SQL").getDao("Socio");
        this.model = socioDAO.get(socioId);

        this.socioId.setText(String.valueOf(model.getSocioId()));
        this.nombre.setText(model.getNombre());
        this.primerApellido.setText(model.getPrimerApellido());
        this.segundoApellido.setText(model.getSegundoApellido());
        this.domicilio.setText(model.getDomicilio());
        this.telefono.setText(model.getTelefono());
        this.email.setText(model.getEmail());

    }

    public void delete(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException, JPAException {
        socioDao.delete(this.model);

        this.goBack(event);
    }

    public void goBack(ActionEvent event) throws IOException {
        URL url = new File("src/main/java/uoc/entreculturas/view/SocioTableView.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent socioView = loader.load();

        Scene tableViewScene = new Scene(socioView);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

    }

    public void update(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException, ParseException {


        this.model.setNombre(nombre.getText());
        this.model.setPrimerApellido(primerApellido.getText());
        this.model.setSegundoApellido(segundoApellido.getText());
        this.model.setDomicilio(domicilio.getText());
        this.model.setTelefono(telefono.getText());
        this.model.setEmail(email.getText());

        socioDao.update(this.model);
        this.goBack(event);
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}

