package uoc.entreculturas.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.dao.DaoFactory;
import uoc.entreculturas.dao.sql.SQLSedeDAO;
import uoc.entreculturas.model.*;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class SedeController implements Initializable {
    private Sede model;
    static SQLSedeDAO sedesDao;

    static {
        try {
            sedesDao = (SQLSedeDAO) DaoFactory.getDaoFactory("SQL").getDao("Sede");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @FXML private TextField location;
    @FXML private CheckBox isCentral;
    @FXML private TableView<Trabajador> trabajadores;
    @FXML private TableColumn trabajadorTelefono;
    @FXML private TableColumn trabajadorNombre;
    @FXML private TableColumn trabajadorApellido;
    @FXML private TableColumn trabajadorId;
    @FXML private TableView<Voluntario> voluntarios;
    @FXML private TableColumn voluntarioTelefono;
    @FXML private TableColumn voluntarioNombre;
    @FXML private TableColumn voluntarioApellido;
    @FXML private TableColumn voluntarioId;
    @FXML private TableView<Colaborador> colaboradores;
    @FXML private TableColumn colaboradorTelefono;
    @FXML private TableColumn colaboradorNombre;
    @FXML private TableColumn colaboradorApellido;
    @FXML private TableColumn colaboradorId;

    public SedeController() {
    }

    public SedeController(Sede model) {
        this.model = model;
    }

    private void addTrabajadores() {
        trabajadorTelefono.setCellValueFactory(
                new PropertyValueFactory<Trabajador,String>("telefono")
        );

        trabajadorNombre.setCellValueFactory(
                new PropertyValueFactory<Trabajador,String>("nombre")
        );

        trabajadorApellido.setCellValueFactory(
                new PropertyValueFactory<Trabajador,String>("primerApellido")
        );

        trabajadorId.setCellValueFactory(
                new PropertyValueFactory<Trabajador,Long>("idTrabajador")
        );

        ObservableList<Trabajador> trabajadoresData = FXCollections.observableArrayList();

        for (Trabajador trabajador : this.model.getTrabajadores()) {
            trabajadoresData.add(trabajador);
        }

        this.trabajadores.setItems(trabajadoresData);
    }

    private void addColaboradores() {
        colaboradorTelefono.setCellValueFactory(
                new PropertyValueFactory<Colaborador,String>("telefono")
        );

        colaboradorNombre.setCellValueFactory(
                new PropertyValueFactory<Colaborador,String>("nombre")
        );

        colaboradorApellido.setCellValueFactory(
                new PropertyValueFactory<Colaborador,String>("primerApellido")
        );

        colaboradorId.setCellValueFactory(
                new PropertyValueFactory<Colaborador,Long>("idColaborador")
        );

        ObservableList<Colaborador> colaboradoresData = FXCollections.observableArrayList();

        for (Colaborador colaborador : this.model.getColaboradores()) {
            colaboradoresData.add(colaborador);
        }

        this.colaboradores.setItems(colaboradoresData);
    }

    private void addVoluntarios() {
        voluntarioTelefono.setCellValueFactory(
                new PropertyValueFactory<Voluntario,String>("telefono")
        );

        voluntarioNombre.setCellValueFactory(
                new PropertyValueFactory<Voluntario,String>("nombre")
        );

        voluntarioApellido.setCellValueFactory(
                new PropertyValueFactory<Voluntario,String>("primerApellido")
        );

        voluntarioId.setCellValueFactory(
                new PropertyValueFactory<Voluntario,Long>("idVoluntario")
        );

        ObservableList<Voluntario> voluntariosData = FXCollections.observableArrayList();

        for (Voluntario voluntario : this.model.getVoluntarios()) {
            voluntariosData.add(voluntario);
        }

        this.voluntarios.setItems(voluntariosData);
    }

    public void setSede(Long id) throws DaoException, JAXBException, SQLException {
        this.model = sedesDao.get(id);

        this.addTrabajadores();
        this.addColaboradores();
        this.addVoluntarios();

        location.setText(model.getLocation());
        isCentral.setSelected(model.getIsCentral());
    }

    public void updateSede(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException {
        if (!location.getText().isEmpty()) {
            this.model.setLocation(location.getText());
            this.model.setIsCentral(isCentral.isSelected());
            sedesDao.update(this.model);
            this.goBack(event);
        }
    }

    public void deleteSede(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException, JPAException {
        sedesDao.delete(this.model);

        this.goBack(event);
    }

    public void goBack(ActionEvent event) throws  IOException {
        URL url = new File("src/main/java/uoc/entreculturas/view/SedesTableView.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent sedeView = loader.load();

        Scene tableViewScene = new Scene(sedeView);

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Set up the table dat
    }
}
