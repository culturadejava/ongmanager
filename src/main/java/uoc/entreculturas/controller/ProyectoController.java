package uoc.entreculturas.controller;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import uoc.entreculturas.JPAException;
import uoc.entreculturas.dao.DaoException;
import uoc.entreculturas.dao.DaoFactory;
import uoc.entreculturas.dao.sql.SQLProyectoDao;
import uoc.entreculturas.dao.sql.SQLSedeDAO;
import uoc.entreculturas.model.Proyecto;
import uoc.entreculturas.model.Sede;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

public class ProyectoController  implements Initializable {

    private Proyecto model;
    static SQLProyectoDao proyectosDao;

    static {
        try {
            proyectosDao = (SQLProyectoDao) DaoFactory.getDaoFactory("SQL").getDao("Proyecto");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML private TextField nombre, codigo, acciones, financiacionAportada, socioLocal, pais, localizacion;
    @FXML private Label id, fechaInicio, fechaFin;


    public ProyectoController() {

        this.nombre = new TextField();
        this.codigo = new TextField();
        this.id = new Label();
        this.acciones = new TextField();
        this.fechaInicio = new Label();
        this.fechaFin = new Label();
        this.financiacionAportada = new TextField();
        this.socioLocal = new TextField();
        this.pais = new TextField();
        this.localizacion = new TextField();
    }

    public ProyectoController(Proyecto model) {

        this.model = model;
        this.nombre = new TextField();
        this.codigo = new TextField();
        this.id = new Label();
        this.acciones = new TextField();
        this.fechaInicio = new Label();
        this.fechaFin = new Label();
        this.financiacionAportada = new TextField();
        this.socioLocal = new TextField();
        this.pais = new TextField();
        this.localizacion = new TextField();
    }



    public void setProyecto(Long id) throws DaoException, JAXBException, SQLException {

        DateFormat formatter=new SimpleDateFormat("dd/MM/yyyy");
        SQLProyectoDao proyectosDAO = (SQLProyectoDao) DaoFactory.getDaoFactory("SQL").getDao("Proyecto");
        this.model = proyectosDAO.get(id);

        this.nombre.setText(model.getNombre());
        this.codigo.setText(model.getCodigo());
        this.id.setText(String.valueOf(model.getId()));
        this.acciones.setText(model.getAcciones());
        this.fechaInicio.setText(formatter.format(model.getFechaInicio()));
        this.fechaFin.setText(formatter.format(model.getFechaFin()));
        this.financiacionAportada.setText(String.valueOf(model.getFinanciacionAportada()));
        this.socioLocal.setText(model.getSocioLocal());
        this.pais.setText(model.getPais());
        this.localizacion.setText(model.getLocalizacion());

    }

    public void delete(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException, JPAException {
        proyectosDao.delete(this.model);

        this.goBack(event);
    }

    public void goBack(ActionEvent event) throws IOException {
        URL url = new File("src/main/java/uoc/entreculturas/view/ProyectoTableView.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent proyectoView = loader.load();

        Scene tableViewScene = new Scene(proyectoView);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();

    }

    public void update(ActionEvent event) throws IOException, DaoException, JAXBException, SQLException, ParseException {

        DateFormat formatter=new SimpleDateFormat("dd/MM/yyyy");

        this.model.setNombre(nombre.getText());
        this.model.setCodigo(codigo.getText());
        this.model.setId(Long.parseLong(id.getText()));
        this.model.setAcciones(acciones.getText());
        this.model.setFechaFin(formatter.parse(fechaFin.getText()));
        this.model.setFechaInicio(formatter.parse(fechaInicio.getText()));
        this.model.setFinanciacionAportada(Float.parseFloat(financiacionAportada.getText()));
        this.model.setLocalizacion(localizacion.getText());
        this.model.setPais(pais.getText());
        this.model.setSocioLocal(socioLocal.getText());
        proyectosDao.update(this.model);
        this.goBack(event);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
