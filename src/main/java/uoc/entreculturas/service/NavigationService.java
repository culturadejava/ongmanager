package uoc.entreculturas.service;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class NavigationService {
    static public void openView( String file, ActionEvent event) throws IOException {
        URL url = new File(file).toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);

        Parent view = loader.load();

        Scene tableViewScene = new Scene(view);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    static public void goHome(ActionEvent event) throws IOException {
        NavigationService.openView("src/main/java/uoc/entreculturas/view/AdminMenuView.fxml", event);
    }
}
