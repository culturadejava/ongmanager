package uoc.entreculturas;

public class JPAException extends Exception{

     /*===================================================
                    CONSTRUCTORES
   =====================================================*/

    public JPAException(String message){

        super(message);
    }


    public JPAException(String message, Throwable cause){
        super(message,cause);

    }

    public JPAException (Throwable cause){

        super(cause);
    }
}
