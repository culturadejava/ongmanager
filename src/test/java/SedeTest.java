import org.junit.Test;
import uoc.entreculturas.model.Sede;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SedeTest {

    @Test
    public void isCentral() {
        Sede central = new Sede(true, "Calle de prueba");
        assertEquals(true, central.getIsCentral());
    }

    @Test
    public void isNotCentral() {
        Sede central = new Sede(false, "Calle de prueba");
        assertEquals(false, central.getIsCentral());
    }

    @Test
    public void setCentral() {
        Sede central  = new Sede("Calle de prueba");

        assertEquals(false, central.getIsCentral());

        central.setIsCentral(true);

        assertEquals(true, central.getIsCentral());
    }

    @Test
    public void getLocalizacion() {
        Sede central = new Sede("Calle de prueba");

        assertEquals("Calle de prueba", central.getLocation());
    }
}